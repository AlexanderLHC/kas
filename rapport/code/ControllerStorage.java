package application.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.Company;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import application.model.Service;
import storage.Storage;

public class ControllerStorage {
	// --------------------------- Company ---------------------------
	public static Company createCompany(String name, int phoneNr) {
		Company company = new Company(name, phoneNr);
		Storage.addCompany(company);
		return company;
	}

	public static void addCompanyToRegistration(Company company, Registration registration) {
		registration.setCompany(company);
	}

	public static ArrayList<Company> getCompanies() {
		return Storage.getCompanies();
	}

	// ---------------------------Conference---------------------------
	public static Conference createConference(String name, int tickets, LocalDate startDate, LocalDate endDate,
			double pricePerDay, String location) {
		Conference conference = new Conference(name, tickets, startDate, endDate, pricePerDay, location);
		Storage.addConference(conference);
		return conference;
	}

	public static Conference updateConference(Conference conference, String name, int tickets, LocalDate startDate,
			LocalDate endDate, double pricePerDay, String location) {
		Storage.updateConference(conference, name, tickets, startDate, endDate, pricePerDay, location);
		return conference;
	}

	public static void removeConference(Conference conference) {
		Storage.removeConference(conference);
	}

	public static void setHotelsToConference(Conference conference, ArrayList<Hotel> hotels) {
		conference.setHotels(hotels);
	}

	public static void addHotelToConference(Conference conference, Hotel hotel) {
		conference.addHotel(hotel);
	}

	public static void setExcursionsToConference(Conference conference, ArrayList<Excursion> excursions) {
		conference.setExcursions(excursions);
	}

	public static void addExcursionToConference(Conference conference, Excursion excursion) {
		conference.addExcursion(excursion);
	}

	public static void removeExcursionFromConference(Conference conference, Excursion excursion) {
		conference.removeExcursion(excursion);
		Storage.removeExcursion(excursion);
	}

	public static ArrayList<Conference> getConferences() {
		return Storage.getConferences();
	}

	// --------------------------- Excursion ---------------------------
	public static Excursion createExcursion(String name, String location, LocalDateTime dateAndTime, double price,
			boolean lunch) {
		Excursion excursion = new Excursion(name, location, dateAndTime, price, lunch);
		Storage.addExcursion(excursion);
		return excursion;
	}

	public static void removeCompanionFromExcursion(Registration registration, Excursion excursion) {
		excursion.removeAttendee(registration);
	}

	public static void deleteExcursion(Conference conference, Excursion excursion) {
		Storage.removeExcursion(excursion);
		conference.removeExcursion(excursion);
	}

	public static ArrayList<Excursion> getExcursions() {
		return Storage.getExcursions();
	}

	// --------------------------- Hotel ---------------------------
	public static Hotel createHotel(String name, String address, double pricePerDaySingle, double pricePerDayDouble) {
		Hotel hotel = new Hotel(name, address, pricePerDaySingle, pricePerDayDouble);
		Storage.addHotel(hotel);
		return hotel;
	}

	public static ArrayList<Hotel> getHotels() {
		return Storage.getHotels();
	}

	public static Hotel updateHotel(Hotel hotel, String name, String address, double pricePerDaySingle,
			double pricePerDayDouble) {

		Storage.updateHotel(hotel, name, address, pricePerDaySingle, pricePerDayDouble);
		return hotel;
	}

	// --------------------------- Registration ---------------------------
	public static Registration createRegistration(Participant participant, Conference conference, Hotel hotel,
			String companion, boolean speaker, Company company, ArrayList<LocalDate> daysAttending) {
		Registration registration = conference.createRegistration(participant, hotel, companion, speaker, company,
				daysAttending);
		Storage.addRegistration(registration);
		return registration;
	}

	public static void updateRegistration(Registration registration, Participant participant, Conference conference,
			Hotel hotel, Company company, boolean speaker, String companion) {
		registration.setParticipant(participant);
		registration.setConference(conference);
		registration.setHotel(hotel);
		registration.setCompany(company);
		registration.setSpeaker(speaker);
		registration.setCompanion(companion);
	}

	public static void removeRegistrationFromConference(Registration registration, Conference conference) {
		if (conference != null) {
			conference.removeRegistration(registration);
		}
	}

	public static void deleteRegistration(Registration registration) {
		Conference conference = registration.getConference();
		if (conference != null) {
			conference.removeRegistration(registration);
		}
		Storage.removeRegistration(registration);
	}

	public static ArrayList<Registration> getRegistrations() {
		return Storage.getRegistrations();
	}

	public static void addExcursionToRegistration(Registration registration, Excursion excursion) {
		registration.addExcursion(excursion);
	}

	public static void addServiceToRegistration(Registration registration, Service service) {
		registration.addService(service);
	}

	public static void removeServiceFromRegistration(Registration registration, Service service) {
		registration.removeService(service);
	}

	// --------------------------- Participant ---------------------------
	public static Participant createParticipant(String name, String address, int phoneNr) {
		Participant participant = new Participant(name, address, phoneNr);
		Storage.addParticipant(participant);
		return participant;
	}

	public static void updateParticipant(Participant participant, String name, String address, int phoneNr) {
		participant.setName(name);
		participant.setAddress(address);
		participant.setPhoneNr(phoneNr);
	}

	public static void removeParticipant(Participant participant) {
		Storage.removeParticipant(participant);
	}

	public static ArrayList<Participant> getParticipants() {
		return Storage.getParticipants();
	}

	// --------------------------- Service ---------------------------
	public static Service createService(String name, Hotel hotel, double price) {
		Service service = hotel.createService(name, price);
		Storage.addService(service);
		return service;
	}

	public static void removeService(Service service) {
		Storage.removeService(service);
	}

	public static ArrayList<Service> getServices() {
		return Storage.getServices();
	}

	// --------------------------- Dummy date ---------------------------
	public static void initDummyData() {
		// Hotels with services
		Hotel h1 = ControllerStorage.createHotel("Bill's Gambling Hall", "Tove Corner Suite 980 Damhaven, WA 57068",
				572.0, 1030.0);
		// ControllerStorage.createService("WIFI", h1, 55);
		ControllerStorage.createService("Morgenmad", h1, 56);
		Hotel h2 = ControllerStorage.createHotel("Excalibur", "Port Trineview, MS 12749", 691, 1053);
		ControllerStorage.createService("WIFI", h2, 49);
		ControllerStorage.createService("Morgenmad", h2, 69);
		Hotel h3 = ControllerStorage.createHotel("Riviera", "North Janni, ND 29767", 569, 860);
		ControllerStorage.createService("WIFI", h3, 26);
		ControllerStorage.createService("Morgenmad", h3, 35);
		Hotel h4 = ControllerStorage.createHotel("Palazzo", "Bachshire, MD 71193", 686, 1262);
		ControllerStorage.createService("WIFI", h4, 19);
		ControllerStorage.createService("Morgenmad", h4, 18);
		Hotel h5 = ControllerStorage.createHotel("Wynn", "Anders Burgs 812, IN 41420", 597, 1473);
		ControllerStorage.createService("WIFI", h5, 70);
		ControllerStorage.createService("Morgenmad", h5, 58);

		// Excursions
		Excursion e1 = ControllerStorage.createExcursion("Museum", "Odense", LocalDateTime.of(2019, 10, 25, 14, 00),
				200, false);
		Excursion e2 = ControllerStorage.createExcursion("Hike", "Odense", LocalDateTime.of(2019, 10, 25, 14, 00), 50,
				true);

		// Conferences
		// 1
		LocalDate startDate1 = LocalDate.of(2019, 10, 25);
		LocalDate endDate1 = LocalDate.of(2019, 10, 28);
		Conference conf1 = ControllerStorage.createConference("Crochet with Greta", 145, startDate1, endDate1, 1297,
				"Mette Extension Suite 217");
		ControllerStorage.addHotelToConference(conf1, h1);
		ControllerStorage.addHotelToConference(conf1, h2);
		ControllerStorage.addExcursionToConference(conf1, e1);
		ControllerStorage.addExcursionToConference(conf1, e2);

		// 2
		LocalDate startDate2 = LocalDate.of(2019, 9, 14);
		LocalDate endDate2 = LocalDate.of(2019, 9, 21);
		Conference conf2 = ControllerStorage.createConference("Green greener greenest", 108, startDate2, endDate2, 1140,
				"Karlsen Harbors Apt. 796");
		ControllerStorage.addHotelToConference(conf2, h2);
		ControllerStorage.addExcursionToConference(conf2, e1);

		// 3
		LocalDate startDate3 = LocalDate.of(2019, 11, 10);
		LocalDate endDate3 = LocalDate.of(2019, 11, 13);
		Conference conf3 = ControllerStorage.createConference("Down with the ravens", 115, startDate3, endDate3, 1193,
				"Lassen Wells Suite 489");
		// 4
		LocalDate startDate4 = LocalDate.of(2019, 11, 13);
		LocalDate endDate4 = LocalDate.of(2019, 11, 17);
		Conference conf4 = ControllerStorage.createConference("Potato meditation", 56, startDate4, endDate4, 1331,
				"New Katja, UT 38846");
		// 5
		LocalDate startDate5 = LocalDate.of(2018, 12, 26);
		LocalDate endDate5 = LocalDate.of(2018, 12, 29);
		Conference conf5 = ControllerStorage.createConference("Karaoke sprouts", 85, startDate5, endDate5, 1252,
				"New Toveview, MN 09511");

		// Companies
		ControllerStorage.createCompany("Siemens", 12345678);
		ControllerStorage.createCompany("LEGO", 87654321);

		// Participants
		// participant 1
		Participant p1 = new Participant("Prof. Berta Jessen", "New Allanchester, MD 53974", 12420721);
		ArrayList<LocalDate> daysAttendingP1C1 = new ArrayList<LocalDate>();
		daysAttendingP1C1.add(LocalDate.of(2019, 10, 25));
		Registration r1 = ControllerStorage.createRegistration(p1, conf1, h1, "Stine", false, null, daysAttendingP1C1);
		ControllerStorage.addExcursionToRegistration(r1, e1);
		ControllerStorage.addServiceToRegistration(r1, h1.getServices().get(0));
		ArrayList<LocalDate> daysAttendingP1C2 = new ArrayList<LocalDate>();
		daysAttendingP1C2.add(LocalDate.of(2019, 9, 14));
		daysAttendingP1C2.add(LocalDate.of(2019, 9, 15));
		Registration r2 = ControllerStorage.createRegistration(p1, conf2, h2, "Hans", true, null, daysAttendingP1C2);
		ControllerStorage.addServiceToRegistration(r2, h2.getServices().get(0));
		ControllerStorage.addServiceToRegistration(r2, h2.getServices().get(1));

		Participant p2 = ControllerStorage.createParticipant("Dr. Tobias Brandt", "Esmareldabury, NH 89413", 157304550);
		Participant p3 = ControllerStorage.createParticipant("Mark Pedersen", "Oliviaborough, WY 40199", 737723949);
		Participant p4 = ControllerStorage.createParticipant("Odette Overgaard", "Lauritsenberg, OK 40303", 34577850);

		Registration r3 = ControllerStorage.createRegistration(p2, conf1, h2, "Hans", false, null, daysAttendingP1C1);
		ControllerStorage.addExcursionToRegistration(r3, e1);
		ControllerStorage.addExcursionToRegistration(r3, e2);
	}

}
