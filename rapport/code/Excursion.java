package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Excursion {
	private String name; // name of excursion
	private String location;// location of excursions
	private LocalDateTime dateAndTime; // date and time
	private double price; // price to participate
	private boolean lunch; // true if lunch is provided otherwise false
	private ArrayList<Registration> attendees; // list of attendees for this excursion

	/**
	 * Creates a new Excursion object.
	 * 
	 * @param name        name of excursion
	 * @param location    location of excursion
	 * @param dateAndTime date and time
	 * @param price       price to participate
	 * @param lunch       true if lunch is provided otherwise false
	 */
	public Excursion(String name, String location, LocalDateTime dateAndTime, double price, boolean lunch) {
		this.name = name;
		this.location = location;
		this.dateAndTime = dateAndTime;
		this.price = price;
		this.lunch = lunch;
		attendees = new ArrayList<>();
	}

	/**
	 * Adds an attendee to the excursion
	 * 
	 * @param name name of attendee
	 */
	public void addAttendee(Registration registration) {
		if (!attendees.contains(registration)) {
			attendees.add(registration);
			registration.addExcursion(this);
		}
	}

	/**
	 * Helper to get companion with name and id.
	 * 
	 * @param registration
	 * @return attendee name with registration id
	 */
	public String attendeeAndID(Registration registration) {
		return registration.getCompanion() + " (" + registration.getID() + ")";
	}

	/**
	 * Removes an attendee to the excursion, if that person is in the list of
	 * attendees
	 * 
	 * @param name name of attendee
	 */
	public void removeAttendee(Registration registration) {
		if (attendees.contains(registration)) {
			attendees.remove(registration);
			registration.removeExcursion(this);
		}
	}

	/**
	 * Returns a LocalDateTime object for when the excursion takes place
	 * 
	 * @return LocalDateTime date and time of the excursion
	 */
	public LocalDateTime getDateAndTime() {
		return dateAndTime;
	}

	/**
	 * Sets the date and time of the excursion
	 * 
	 * @param dateAndTime new date and time
	 */
	public void setDateAndTime(LocalDateTime dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	/**
	 * Returns an ArrayList containing the names of the attendees
	 * 
	 * @return names of attendees
	 */
	public ArrayList<Registration> getRegistrations() {
		return attendees;
	}

	public ArrayList<String> getAttendees() {
		ArrayList<String> companionsWithID = new ArrayList<>();
		for (Registration r : attendees) {
			companionsWithID.add(attendeeAndID(r));
		}
		return companionsWithID;
	}

	/**
	 * Returns the location of the excursion
	 * 
	 * @return location of excursion
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Returns the name of the excursion
	 * 
	 * @return name of excursion
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the price to participate
	 * 
	 * @return price of the excursion
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Returns a boolean to indicate whether lunch is provieded
	 * 
	 * @return true if lunch is provided otherwise return false
	 */
	public boolean hasLunch() {
		return lunch;
	}

	/**
	 * Sets the location of the excursion
	 * 
	 * @param location new location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Sets the boolean value for if lunch is provided
	 * 
	 * @param lunch if lunch is provided
	 */
	public void setLunch(boolean lunch) {
		this.lunch = lunch;
	}

	/**
	 * Sets the name of the excursion
	 * 
	 * @param name new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the price to participate
	 * 
	 * @param price new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return String.format("%s (%.2f kr)", name, price);
	}
}
