package gui;

import javafx.scene.Group;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class ButtonAdd extends Group {

	public ButtonAdd(String id, int size) {
		Circle button = new Circle(size);
		button.setId(id);
		int end = size - 5;
		int start = end * -1;
		Line lHorizontal = new Line(0, start, 0, end);
		lHorizontal.setStrokeWidth(2);
		Line lVertical = new Line(start, 0, end, 0);
		lVertical.setStrokeWidth(2);
		this.getChildren().addAll(button, lHorizontal, lVertical);
	}
}
