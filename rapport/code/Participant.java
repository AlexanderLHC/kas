package application.model;

public class Participant {
	private String name;
	private String address;
	private int phoneNr;

	/**
	 * Creates a new Participant object
	 * 
	 * @param name
	 * @param address
	 * @param company
	 * @param phoneNr
	 */

	public Participant(String name, String address, int phoneNr) {
		this.name = name;
		this.address = address;
		this.phoneNr = phoneNr;
	}

	/**
	 * Returns the name of the participant
	 * 
	 * @return String name of participant
	 */

	public String getName() {
		return name;
	}

	/**
	 * Returns the address of the participant
	 * 
	 * @return String address of participant
	 */

	public String getAddress() {
		return address;
	}

	/**
	 * Returns the phone number of the participant
	 * 
	 * @return int phoneNr of participant
	 */

	public int getPhoneNr() {
		return phoneNr;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhoneNr(int phoneNr) {
		this.phoneNr = phoneNr;
	}

	@Override
	public String toString() {
		return name;
	}

}
