package application.model;

public class Company {
	private String name;
	private int phoneNr;

	/**
	 * Creates a new Company object
	 * 
	 * @param name
	 * @param phoneNr
	 */

	public Company(String name, int phoneNr) {
		this.name = name;
		this.phoneNr = phoneNr;
	}

	/**
	 * Returns the name of the company
	 * 
	 * @return String name of company
	 */

	public String getName() {
		return name;
	}

	/**
	 * Returns the phone number of the company
	 * 
	 * @return int phoneNr of company
	 */

	public int getPhoneNr() {
		return phoneNr;
	}

	public String toString() {
		return name;
	}
}
