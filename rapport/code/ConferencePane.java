package gui;

import java.time.LocalDate;

import application.controller.ControllerGUI;
import application.controller.ControllerStorage;
import application.model.Conference;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ConferencePane extends ScrollPane {
	private ListView lvwRegistrations = new ListView();
	private Button btnCreateRegistration, btnRemoveRegistration;
	private Label lblParticipants;
	private Conference conference;
	private VBox vbConference = new VBox();
	private Button btnEdit;
	private boolean editable;
	private TextField txfName, txfLocation, txfTickets, txfPricePerDay;
	private DatePicker startDate, endDate;
	private Text txError;

	public ConferencePane(Conference conference) {
		vbConference.getStyleClass().add("card");
		vbConference.getStyleClass().add("card-conference");
		vbConference.getStyleClass().add("card-pane");
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		vbConference.getStylesheets().add(getClass().getResource("ConferenceHotelPane.css").toExternalForm());
		this.conference = conference;
		editable = false;
		initContent();
		HBox hbCenter = new HBox(vbConference);
		hbCenter.setAlignment(Pos.CENTER);
		this.setContent(hbCenter);
	}

	private void initContent() {
		VBox vbContent = new VBox();
		vbContent.setId("content");

		Label lblTitle = new Label("Conference: ");
		lblTitle.getStyleClass().add("headline");
		txfName = new TextField(conference.getName());
		btnEdit = new Button("");
		btnEdit.setId("edit");
		btnEdit.setOnAction(e -> onActionSetEdit());
		Button btnDelete = new Button("");
		btnDelete.getStyleClass().add("btn-delete");
		btnDelete.setOnAction(e -> onActionSetEdit());
		HBox hbName = new HBox(lblTitle, txfName, btnEdit, btnDelete);
		hbName.getStyleClass().add("headline");
		hbName.setAlignment(Pos.CENTER);
		vbContent.getChildren().addAll(hbName);

		Label lblLocation = new Label("Location:");
		txfLocation = new TextField(conference.getLocation());
		HBox hbLocation = new HBox(lblLocation, txfLocation);
		vbContent.getChildren().add(hbLocation);

		Label lblTickets = new Label("Tickets:");
		Label lblTicketsSold = new Label(conference.getTicketsSold() + "/");
		txfTickets = new TextField(conference.getTickets() + "");

		HBox hbTickets = new HBox(lblTickets, lblTicketsSold, txfTickets);
		vbContent.getChildren().add(hbTickets);

		Label lblPricePerDay = new Label("Price per day:");
		txfPricePerDay = new TextField(conference.getPricePerDay() + "");
		HBox hbPricePerDay = new HBox(lblPricePerDay, txfPricePerDay);
		vbContent.getChildren().add(hbPricePerDay);

		Label lblDate = new Label("Date:");
		startDate = new DatePicker(conference.getStartDate());
		endDate = new DatePicker(conference.getStartDate());
		HBox hbDates = new HBox(lblDate, startDate, endDate);
		vbContent.getChildren().add(hbDates);
		vbConference.getChildren().add(vbContent);

		txError = new Text("");
		HBox hbError = new HBox(txError);
		hbError.getStyleClass().add("text-error");
		vbConference.getChildren().add(hbError);

		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		vbConference.getChildren().add(tabPane);

		onActionSetEdit();
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabPane.setMinHeight(500);

		Tab tabHotels = new Tab("Hotels");
		tabHotels.setContent(new ConferenceHotelPane(conference));
		tabPane.getTabs().add(tabHotels);

		Tab tabParticipants = new Tab("Participants");
		ParticipantPane participantPane = new ParticipantPane(conference);
		tabParticipants.setContent(participantPane);
		tabParticipants.setOnSelectionChanged(event -> participantPane.updateParticipantList());
		tabPane.getTabs().add(tabParticipants);

		Tab tabCreateRegistration = new Tab("Create Registration");
		tabPane.getTabs().add(tabCreateRegistration);
		RegistrationPane registrationPane = new RegistrationPane(conference);
		tabCreateRegistration.setContent(registrationPane);

		Tab tabExcursions = new Tab("Excursions");
		tabPane.getTabs().add(tabExcursions);
		ExcursionPane excursionPane = new ExcursionPane(conference);
		tabExcursions.setContent(excursionPane);
	}

	private void onActionSetEdit() {
		VBox gpContent = (VBox) vbConference.lookup("#content");
		for (Node node : gpContent.getChildren()) {
			TextField txfNode = (TextField) node.lookup(".text-input");
			try {
				txfNode.setEditable(editable);
			} catch (NullPointerException e) {
			}
			btnEdit.setId("edit");
		}
		if (editable) {
			btnEdit.setId("save");
			btnEdit.setOnAction(e -> onActionSubmit());
		} else {
			btnEdit.setId("edit");
			btnEdit.setOnAction(e -> onActionSetEdit());
		}
		editable = !editable;
	}

	private void onActionSubmit() {
		boolean hasError = false;
		String name = txfName.getText();
		String location = txfLocation.getText();
		String tickets = txfTickets.getText();
		String pricePerDay = txfPricePerDay.getText();
		LocalDate startDateNew = startDate.getValue();
		LocalDate endDateNew = endDate.getValue();
		StringBuilder sbError = new StringBuilder();

		if (name.isEmpty() || name.length() < 3) {
			sbError.append("* name can't be empty or less than 3 characters.\n");
			hasError = true;
		}
		if (location.isEmpty()) {
			sbError.append("* location can't be empty.\n");
			hasError = true;
		}
		if (tickets.isEmpty() || !tickets.matches("\\d+")) { // not empty and just integers
			sbError.append("* tickets can't be empty and only integers.\n");
			hasError = true;
		}
		if (pricePerDay.isEmpty() || !pricePerDay.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* pricer per day can't be empty and only integers.\n");
			hasError = true;
		}
		if (!hasError) {
			Conference conferenceUpdated = ControllerStorage.updateConference(conference, name,
					Integer.parseInt(tickets), startDateNew, endDateNew, Double.parseDouble(pricePerDay), location);
			ControllerGUI.openConferenceView(conferenceUpdated);
		} else {
			txError.setText(sbError.toString());
		}

	}
}
