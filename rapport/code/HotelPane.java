package gui;

import application.controller.ControllerGUI;
import application.controller.ControllerStorage;
import application.model.Hotel;
import application.model.Service;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HotelPane extends ScrollPane {
	private VBox vbHotel = new VBox();
	private Hotel hotel;
	private Button btnEdit;
	private boolean editable;
	private TextField txfName, txfAddress, txfPricePerDaySingle, txfPricePerDayDouble, txfServiceName, txfServicePrice;
	private Text txError;
	private HBox hbServiceAdder;
	private ListView<String>  lwServices;

	public HotelPane(Hotel hotel) {
		vbHotel.getStyleClass().add("card");
		vbHotel.getStyleClass().add("card-conference");
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		vbHotel.getStylesheets().add(getClass().getResource("ConferenceHotelPane.css").toExternalForm());
		this.hotel = hotel;
		editable = false;
		initContent();
		HBox hbCenter = new HBox(vbHotel);
		hbCenter.setAlignment(Pos.CENTER);
		this.setContent(hbCenter);
		onActionSetEdit();
	}

	private void initContent() {
		btnEdit = new Button("");
		btnEdit.setId("edit");
		btnEdit.setOnAction(e -> onActionSetEdit());

		Label lblName = new Label("Name: ");
		txfName = new TextField(hotel.getName());
		HBox hbTitleName = new HBox(lblName, txfName, btnEdit);
		hbTitleName.getStyleClass().add("headline");
		vbHotel.getChildren().add(hbTitleName);

		Label lblAddress = new Label("Address: ");
		txfAddress = new TextField(hotel.getAddress());
		HBox hbAddress = new HBox(lblAddress, txfAddress);
		vbHotel.getChildren().add(hbAddress);

		Label lblPricePerDaySingle = new Label("Price per day single (kr): ");
		txfPricePerDaySingle = new TextField(hotel.getPricePerDaySingle() + "");
		HBox hbPricePerDaySingle = new HBox(lblPricePerDaySingle, txfPricePerDaySingle);
		vbHotel.getChildren().add(hbPricePerDaySingle);

		Label lblPricePerDayDouble = new Label("Price per day double (kr): ");
		txfPricePerDayDouble = new TextField(hotel.getPricePerDayDouble() + "");
		HBox hbPricePerDayDouble = new HBox(lblPricePerDayDouble, txfPricePerDayDouble);
		vbHotel.getChildren().add(hbPricePerDayDouble);

		Label lblServices = new Label("Services");
		lwServices = new ListView<>();
		for (Service s : hotel.getServices()) {
			lwServices.getItems().add(s.toString());
		}
		Label lblServiceName = new Label("Service name: ");
		txfServiceName = new TextField("");
		Label lblServicePrice = new Label("price: ");
		txfServicePrice = new TextField("");
		Button btnAddService = new Button("Add Service");
		btnAddService.setOnAction(e -> onActionAddService());
		hbServiceAdder = new HBox(lblServiceName, txfServiceName,lblServicePrice, txfServicePrice, btnAddService);
		hbServiceAdder.setVisible(false);
		
		txError = new Text("");
		vbHotel.getChildren().addAll(lblServices, lwServices, hbServiceAdder, txError);
		

	}

	private void onActionSetEdit() {
		for (Node node : vbHotel.getChildren()) {
			TextField txfNode = (TextField) node.lookup(".text-input");
			try {
				txfNode.setEditable(editable);
			} catch (NullPointerException e) {
			}
			btnEdit.setId("edit");
		}
		hbServiceAdder.setVisible(editable);
		
		if (editable) {
			btnEdit.setId("save");
			btnEdit.setOnAction(e -> onActionSubmit());
		} else {
			btnEdit.setId("edit");
			btnEdit.setOnAction(e -> onActionSetEdit());
		}
		editable = !editable;
	}
	
	private void onActionAddService() {
		String name = txfServiceName.getText();
		String price = txfServicePrice.getText();
		StringBuilder sbError = new StringBuilder();
		boolean hasError = false;

		if (name.isEmpty()) {
			sbError.append("* name: can't be empty.");
			hasError = true;
		}
		if (price.isEmpty()
				|| !price.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* price: can't be empty and only integers.");
			hasError = true;
		}

		if (!hasError) {
			Service s = ControllerStorage.createService(name, hotel, Double.parseDouble(price));
			lwServices.getItems().add(s.toString());
			txfServiceName.setText("");
			txfServicePrice.setText("");
		} else {
			txError.setText(sbError.toString());
		}

	}

	private void onActionSubmit() {
		boolean hasError = false;
		String name = txfName.getText();
		String address = txfAddress.getText();
		String pricePerDaySingle = txfPricePerDaySingle.getText();
		String pricePerDayDouble = txfPricePerDayDouble.getText();
		StringBuilder sbError = new StringBuilder();

		if (name.isEmpty() || name.length() < 3) {
			sbError.append("* name can't be empty or less than 3 characters.\n");
			hasError = true;
		}
		if (address.isEmpty()) {
			sbError.append("* address can't be empty.\n");
			hasError = true;
		}
		if (pricePerDaySingle.isEmpty()
				|| !pricePerDaySingle.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* pricer per day single can't be empty and only integers.\n");
			hasError = true;
		}
		if (pricePerDayDouble.isEmpty()
				|| !pricePerDayDouble.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* pricer per day double can't be empty and only integers.\n");
			hasError = true;
		}
		if (!hasError) {
			Hotel hotelUpdated = ControllerStorage.updateHotel(hotel, name, address,
					Double.parseDouble(pricePerDaySingle), Double.parseDouble(pricePerDayDouble));
			ControllerGUI.openHotelView(hotelUpdated);
		} else {
			txError.setText(sbError.toString());
		}
	}

}
