package gui;

import application.controller.ControllerGUI;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class App extends Application {
	public static BorderPane pane;
	private static Scene scene;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Conference Adminstration System");
		pane = new BorderPane();
		pane.getStyleClass().add("app");
		scene = new Scene(pane, 750, 600);

		this.initContent(pane);
		this.initHotKeys();

		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void initContent(BorderPane pane) {
		initHeader();
		initCenter();
		pane.getCenter().getStyleClass().add("body");
	}

	private void initHeader() {
		BorderPane header = new BorderPane();
		header.getStyleClass().add("header");
		Label lblTitle = new Label("Conference Administration System");
		header.setLeft(lblTitle);

		HBox hbNavButtons = new HBox();
		hbNavButtons.getStyleClass().add("nav-buttons");

		// conferences
		Button btnConference = new Button("");
		btnConference.getStyleClass().add("btn-conference");
		btnConference.setOnAction(e -> ControllerGUI.onClickViewConferences());
		btnConference.setTooltip(new Tooltip("Conference list"));
		hbNavButtons.getChildren().add(btnConference);

		// hotels
		Button btnHotel = new Button("");
		btnHotel.getStyleClass().add("btn-hotel");
		btnHotel.setOnAction(e -> ControllerGUI.onClickViewHotels());
		btnHotel.setTooltip(new Tooltip("Hotel list"));
		hbNavButtons.getChildren().add(btnHotel);

		// help
		Button btnHelp = new Button("");
		btnHelp.getStyleClass().add("btn-help");
		btnHelp.setOnAction(e -> ControllerGUI.onClickViewHelp());
		btnHelp.setTooltip(new Tooltip("Get help!"));
		hbNavButtons.getChildren().add(btnHelp);

		// dummy
		Button btnDummy = new Button("");
		btnDummy.getStyleClass().add("btn-dummy");
		btnDummy.setOnAction(e -> ControllerGUI.onClickDummyData());
		btnDummy.setTooltip(new Tooltip("Add dummy data"));
		hbNavButtons.getChildren().add(btnDummy);

		header.setCenter(hbNavButtons);
		pane.setTop(header);
	}

	private void initHotKeys() {
		KeyCombination kcF1 = new KeyCodeCombination(KeyCode.F1);
		Runnable rnKeyF1 = () -> ControllerGUI.onClickViewConferences();
		scene.getAccelerators().put(kcF1, rnKeyF1);

		KeyCombination kcF2 = new KeyCodeCombination(KeyCode.F2);
		Runnable rnKeyF2 = () -> ControllerGUI.onClickViewHotels();
		scene.getAccelerators().put(kcF2, rnKeyF2);

		KeyCombination kcF3 = new KeyCodeCombination(KeyCode.F3);
		Runnable rnKeyF3 = () -> ControllerGUI.onClickViewHelp();
		scene.getAccelerators().put(kcF3, rnKeyF3);
	}

	private void initCenter() {
		ControllerGUI.onClickViewConferences();
	}

	public static Scene getScene() {
		return scene;
	}

}
