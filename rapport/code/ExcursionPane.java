package gui;

import java.util.HashMap;

import application.controller.ControllerStorage;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import gui.ExcursionCreateWindow;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import storage.Storage;

public class ExcursionPane extends GridPane {
	private ListView<Excursion> lvwExcursions = new ListView<>();
	private ListView<String> lvwAttendees = new ListView<>();
	private HashMap<String, Registration> hmCompanionToRegistration = new HashMap<>();
	private Conference conference;

	public ExcursionPane(Conference conference) {
		this.conference = conference;
		Label lblExcursions = new Label("Excursions:");
		this.add(lblExcursions, 0, 0);

		this.add(lvwExcursions, 0, 1, 1, 4);
		lvwExcursions.setPrefWidth(200);
		lvwExcursions.setPrefHeight(200);

		ChangeListener<Excursion> listener = (ov, o, n) -> this.excursionItemSelected();
		lvwExcursions.getItems().setAll(conference.getExcursions());
		lvwExcursions.getSelectionModel().selectedItemProperty().addListener(listener);

		Label lblParticipants = new Label("Participants:");
		this.add(lblParticipants, 1, 0);

		this.add(lvwAttendees, 1, 1, 1, 4);
		lvwAttendees.setPrefWidth(200);
		lvwAttendees.setPrefHeight(200);

		Button btnCreateExcursions = new Button();
		btnCreateExcursions.setText("Create Excursion");
		this.add(btnCreateExcursions, 2, 1);
		btnCreateExcursions.setOnAction(event -> createAction());

		Button btnDeleteExcursions = new Button();
		btnDeleteExcursions.setText("Delete Excursion");
		this.add(btnDeleteExcursions, 2, 2);
		btnDeleteExcursions.setOnAction(event -> deleteAction());

		Button btnRemoveParticipant = new Button();
		btnRemoveParticipant.setText("Remove Participant");
		this.add(btnRemoveParticipant, 2, 3);
		btnRemoveParticipant.setOnAction(event -> removeParticipantAction());
	}

	private void removeParticipantAction() {
		String attendee = lvwAttendees.getSelectionModel().getSelectedItem();
		Excursion excursion = lvwExcursions.getSelectionModel().getSelectedItem();
		if (attendee == null) {
			Alert errorAlert = new Alert(AlertType.ERROR);
			errorAlert.setTitle("Error");
			errorAlert.setHeaderText("An error occured. Please see below.");
			errorAlert.setContentText("No attendee selected. Please select an attendee and try again.");
			errorAlert.show();
		} else {
			Alert confirmAlert = new Alert(AlertType.CONFIRMATION,
					"Are you sure?", ButtonType.YES,
					ButtonType.NO);
			confirmAlert.setTitle("Remove attendee");
			confirmAlert.setHeaderText("You are about to remove attendee " + attendee + " from " + excursion.getName());
			confirmAlert.showAndWait();

			if (confirmAlert.getResult() == ButtonType.YES) {
				Registration registration = hmCompanionToRegistration.get(attendee);
				ControllerStorage.removeCompanionFromExcursion(registration, excursion);
				excursionItemSelected();
			}
		}

	}

	private void deleteAction() {
		Excursion excursion = lvwExcursions.getSelectionModel().getSelectedItem();
		if (excursion == null) {
			Alert errorAlert = new Alert(AlertType.ERROR);
			errorAlert.setTitle("Error");
			errorAlert.setHeaderText("An error occured. Please see below.");
			errorAlert.setContentText("No excursion selected. Please select an excursion and try again.");
			errorAlert.show();
		} else {
			Alert confirmAlert = new Alert(AlertType.CONFIRMATION,
					"Are you sure?", ButtonType.YES, ButtonType.NO);
			confirmAlert.setTitle("Delete excursion");
			confirmAlert.setHeaderText("You are about to delete excursion " + excursion.getName());
			confirmAlert.showAndWait();

			if (confirmAlert.getResult() == ButtonType.YES) {
				ControllerStorage.deleteExcursion(conference, lvwExcursions.getSelectionModel().getSelectedItem());
				updateExcursionList();
				excursionItemSelected();
			}
		}
	}

	private void excursionItemSelected() {
		Excursion excursion = lvwExcursions.getSelectionModel().getSelectedItem();
		if (excursion != null) {
			lvwAttendees.getItems().setAll(excursion.getAttendees());
			for (Registration r : excursion.getRegistrations()) {
				hmCompanionToRegistration.put(excursion.attendeeAndID(r), r);
			}
		} else {
			lvwAttendees.getItems().setAll("");
		}
	}

	private void createAction() {
		ExcursionCreateWindow dia = new ExcursionCreateWindow("Create excursion", conference);
		dia.showAndWait();

		// Wait for the modal dialog to close

		lvwExcursions.getItems().setAll(ControllerStorage.getExcursions());
		int index = lvwExcursions.getItems().size() - 1;
		lvwExcursions.getSelectionModel().select(index);
	}

	private void updateExcursionList() {
		lvwExcursions.getItems().setAll(conference.getExcursions());

	}
}
