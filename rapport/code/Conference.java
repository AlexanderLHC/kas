package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Conference {
	private String name; // name of conferences
	private int tickets; // number of tickets
	private LocalDate startDate; // start date of the conference
	private LocalDate endDate; // end date of the conference
	private double pricePerDay; // price to participate in the conference per day
	private String location; // address of the conference
	private ArrayList<Hotel> hotels; // list of hotels
	private ArrayList<Excursion> excursions; // list of excursions
	private ArrayList<Registration> registrations; // list of participants' registrations

	private int participantID = 1;

	/**
	 * Creates new Conference object
	 * 
	 * @param name        name of the conference
	 * @param tickets     number of tickets
	 * @param startDate   date the conference starts
	 * @param endDate     date the conference ends
	 * @param pricePerDay price to participate per day
	 * @param location    address of the conference
	 */
	public Conference(String name, int tickets, LocalDate startDate, LocalDate endDate, double pricePerDay,
			String location) {
		this.name = name;
		this.tickets = tickets;
		this.startDate = startDate;
		this.endDate = endDate;
		this.pricePerDay = pricePerDay;
		this.location = location;
		hotels = new ArrayList<>();
		excursions = new ArrayList<>();
		registrations = new ArrayList<>();
	}

	public void increaseParticipantID() {
		participantID++;
	}

	public int getParticipantID() {
		return participantID;
	}

	/**
	 * Adds a hotel to the list of hotels
	 * 
	 * @param hotel hotel to be added
	 */
	public void addHotel(Hotel hotel) {
		if (!hotels.contains(hotel)) {
			hotels.add(hotel);
		}
	}

	/**
	 * Removes a hotel from the list of hotels
	 * 
	 * @param hotel hotel to be removed
	 */
	public void removeHotel(Hotel hotel) {
		if (hotels.contains(hotel)) {
			hotels.remove(hotel);
		}
	}

	/**
	 * Adds an excursion to the list of excursions
	 * 
	 * @param excursion excursion to be added
	 */
	public void addExcursion(Excursion excursion) {
		if (!excursions.contains(excursion)) {
			excursions.add(excursion);
		}
	}

	/**
	 * Removes an excursion from the list of excursions and also updates the
	 * registrations with same excursion.
	 * 
	 * @param excursion excursion to be removed
	 */
	public void removeExcursion(Excursion excursion) {
		if (excursions.contains(excursion)) {
			excursions.remove(excursion);
			for (Registration r : registrations) {
				if (r.getExcursions().contains(excursion)) {
					r.removeExcursion(excursion);
				}
			}
		}
	}

	/**
	 * Create a registration and adds it to the list of registrations
	 * 
	 * @param registraion registration to be added
	 * @return the new registration
	 */
	public Registration createRegistration(Participant participant, Hotel hotel, String companion, boolean speaker,
			Company company, ArrayList<LocalDate> daysAttending) {
		Registration registration = new Registration(participant, this, hotel, companion, speaker, company,
				daysAttending);
		registrations.add(registration);
		return registration;
	}

	/**
	 * Removes a registration from the list of registrations
	 * 
	 * @param registration registration to be removed
	 */
	public void removeRegistration(Registration registration) {
		if (registrations.remove(registration)) {
			registrations.remove(registration);
			registration.setConference(null);
		}
	}

	/**
	 * Returns a list of all the participants registered
	 * 
	 * @return list of participants
	 */
	public ArrayList<Participant> getParticipants() {
		ArrayList<Participant> participants = new ArrayList<>();
		for (Registration registration : registrations) {
			Participant participant = registration.getParticipant();
			participants.add(participant);
		}
		return participants;
	}

	/**
	 * Returns the number of tickets sold
	 * 
	 * @return number of tickets sold
	 */
	public int getTicketsSold() {
		return registrations.size();
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public String getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}

	public double getPricePerDay() {
		return pricePerDay;
	}

	public ArrayList<Registration> getRegistrations() {
		return registrations;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public int getTickets() {
		return tickets;
	}

	public int getDuration() {
		return endDate.compareTo(startDate);
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public void setExcursions(ArrayList<Excursion> excursions) {
		this.excursions = excursions;
	}

	public void setHotels(ArrayList<Hotel> hotels) {
		this.hotels = hotels;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPricePerDay(double pricePerDay) {
		this.pricePerDay = pricePerDay;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setTickets(int tickets) {
		this.tickets = tickets;
	}

	public ArrayList<Hotel> getHotels() {
		return hotels;
	}

	public ArrayList<Excursion> getExcursions() {
		return excursions;
	}
}
