package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import application.controller.ControllerGUI;
import application.controller.ControllerStorage;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ConferenceCreatePane extends ScrollPane {
	private Label lblName, lblLocation, lblTickets, lblDate, lblPricePerDay;
	private Label lblNameError, lblLocationError, lblTicketsError, lblDateError, lblPricePerDayError;
	private TextField txfName, txfLocation, txfTickets, txfPricePerDay;
	private DatePicker dpStart, dpEnd;
	private GridPane gpContentPane = new GridPane();
	private ListView<Hotel> lwHotels = new ListView<>();
	private ListView<Hotel> lwCreatedHotels;
	private ListView<Excursion> lwExcursions = new ListView<>();
	
	public ConferenceCreatePane() {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		gpContentPane.getStylesheets().add(getClass().getResource("ConferenceHotelPane.css").toExternalForm());
		gpContentPane.getStyleClass().add("card");
		gpContentPane.getStyleClass().add("card-conference");

		// Save Hotkey
		KeyCombination kcCtrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
		Runnable rnKeyCtrlS = () -> onClickCreateConference();
		App.getScene().getAccelerators().put(kcCtrlS, rnKeyCtrlS);
		
		initContent();
	}

	private void initContent() {
		Label lblHeadline = new Label("Create new conference");
		HBox hbHeadline = new HBox(lblHeadline);
		hbHeadline.getStyleClass().add("headline");
		gpContentPane.add(hbHeadline, 0, 0, 4, 1);

		lblName = new Label("Conference name: ");
		txfName = new TextField();
		lblNameError = new Label("");
		gpContentPane.add(lblName, 0, 1);
		gpContentPane.add(txfName, 1, 1);
		gpContentPane.add(lblNameError, 2, 1);

		lblLocation = new Label("Location:");
		txfLocation = new TextField();
		lblLocationError = new Label("");
		gpContentPane.add(lblLocation, 0, 2);
		gpContentPane.add(txfLocation, 1, 2);
		gpContentPane.add(lblLocationError, 2, 2);

		lblTickets = new Label("Tickets available:");
		txfTickets = new TextField();
		lblTicketsError = new Label("");
		gpContentPane.add(lblTickets, 0, 3);
		gpContentPane.add(txfTickets, 1, 3);
		gpContentPane.add(lblTicketsError, 2, 3);
		txfTickets.focusedProperty().addListener((arg0, oldValue, newValue) -> {
			if (!newValue) {
				if (!txfTickets.getText().matches("\\d+")) {
					lblTicketsError.setText("only integers.");
				} else {
					lblTicketsError.setText("");
				}
			}

		});

		lblDate = new Label("Dates (from - to):");
		lblDateError = new Label("");
		dpStart = new DatePicker();
		dpEnd = new DatePicker();
		dpStart.setValue(LocalDate.now());
		dpEnd.setValue(LocalDate.now());
		gpContentPane.add(lblDate, 0, 4);
		gpContentPane.add(dpStart, 1, 4);
		gpContentPane.add(dpEnd, 2, 4);
		gpContentPane.add(lblDateError, 3, 4);

		lblPricePerDay = new Label("Price per day (kr):");
		txfPricePerDay = new TextField();
		lblPricePerDayError = new Label("");
		gpContentPane.add(lblPricePerDay, 0, 5);
		gpContentPane.add(txfPricePerDay, 1, 5);
		gpContentPane.add(lblPricePerDayError, 2, 5);;
		txfPricePerDay.focusedProperty().addListener((arg0, oldValue, newValue) -> {
			if (!newValue) {
				if (!txfPricePerDay.getText().matches("\\d+")) {
					lblPricePerDayError.setText("only integers.");
				} else {
					lblPricePerDayError.setText("");
				}
			}

		});

		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		gpContentPane.add(tabPane, 0, 6, 4, 4);

		// Buttons
		HBox hbButtons = new HBox();
		Button btnAdd = new Button("Add");
		btnAdd.setOnAction(e -> this.onClickCreateConference());
		Button btnAddMore = new Button("Add more");
		btnAddMore.setOnAction(e -> this.onClickCreateAnotherConference());
		Button btnCancel = new Button("Cancel");
		btnCancel.setOnAction(e -> ControllerGUI.onClickViewConferences());
		hbButtons.getChildren().addAll(btnAdd, btnAddMore, btnCancel);
		gpContentPane.add(hbButtons, 0, 10, 4, 1);

		HBox hbCenter = new HBox(gpContentPane);
		hbCenter.setAlignment(Pos.CENTER);
		this.setContent(hbCenter);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabHotels = new Tab("Hotels");
		tabPane.getTabs().add(tabHotels);
		tabHotels.setContent(tabHotels());
		Tab tabExcursions = new Tab("Excursions");
		tabPane.getTabs().add(tabExcursions);
		tabExcursions.setContent(tabExcursions());
	}

	private GridPane tabHotels() {
		GridPane gpTabHotels = new GridPane();
		Button btnCreateHotel = new Button("Create hotel");
		btnCreateHotel.setOnAction(e -> onActionCreateHotel());
		Button btnAddHotel = new Button("Add selected");
		btnAddHotel.setOnAction(e -> onActionAddHotel());
		HBox hbButtons = new HBox(btnCreateHotel,btnAddHotel );
		lwCreatedHotels = new ListView<Hotel>();
		lwCreatedHotels.getItems().setAll(ControllerStorage.getHotels());
		gpTabHotels.add(hbButtons, 0, 0);
		HBox hbHotelsAdded = new HBox(new Label("Hotels added"));
		HBox hbHotelsAvailable = new HBox(new Label("Hotels available"));
		gpTabHotels.add(hbHotelsAdded, 0, 1);
		gpTabHotels.add(hbHotelsAvailable, 1, 1);
		gpTabHotels.add(lwHotels, 0, 2);
		gpTabHotels.add(lwCreatedHotels, 1, 2);
		return gpTabHotels;
	}

	private GridPane tabExcursions() {
		GridPane gpTabExcursions = new GridPane();
		Label lblInfo = new Label("Excursions can be added after a conference has been created");
		gpTabExcursions.add(lblInfo, 0, 0);
		return gpTabExcursions;
	}

	private boolean createConference() {
		boolean hasError = false;
		String name = txfName.getText();
		String location = txfLocation.getText();
		String tickets = txfTickets.getText();
		LocalDate startDate = dpStart.getValue();
		LocalDate endDate = dpEnd.getValue();
		String pricePerDay = txfPricePerDay.getText();

		if (name.isEmpty() || name.length() < 3) {
			lblNameError.setText("* name can't be empty or less than 3 characters.");
			hasError = true;
		}
		if (location.isEmpty()) {
			lblLocationError.setText("* location can't be empty.");
			hasError = true;
		}
		if (tickets.isEmpty() || !tickets.matches("\\d+")) { // not empty and just integers
			lblTicketsError.setText("* tickets can't be empty and only integers.");
			hasError = true;
		}
		if (pricePerDay.isEmpty()
				|| !pricePerDay.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			lblPricePerDayError.setText("* price per day can't be empty and only integers.");
			hasError = true;
		}
		if (!hasError) {
			Conference conference = ControllerGUI.onClickCreateConference(this, name, location, Integer.parseInt(tickets), startDate, endDate,
					Double.parseDouble(pricePerDay));
				ArrayList<Hotel> hotels = new ArrayList<Hotel>(lwHotels.getItems());
				ControllerStorage.setHotelsToConference(conference, hotels );
		}
		return !hasError;
	}
	

	private void onActionCreateHotel() {
		Stage createHotelDia = new Stage();

		createHotelDia.initStyle(StageStyle.UTILITY);
		createHotelDia.initModality(Modality.APPLICATION_MODAL);
		createHotelDia.setResizable(false);
		createHotelDia.setTitle("Create Hotel");
		

		HotelCreatePane hotelCreatePane = new HotelCreatePane();
		HBox hbHotelCreate = (HBox) hotelCreatePane.getContent();
		Button btnAdd = (Button) hbHotelCreate.lookup("#add");
		btnAdd.setOnAction(e-> onDiaHotelDialogCreateAction(hotelCreatePane, createHotelDia));
		Button btnAddMore = (Button) hbHotelCreate.lookup("#addMore");
		btnAddMore.setVisible(false);
		Button btnCancel = (Button) hbHotelCreate.lookup("#cancel");
		btnCancel.setOnAction(event -> createHotelDia.hide());

		Scene scene = new Scene(hotelCreatePane);

		createHotelDia.setScene(scene);
		createHotelDia.showAndWait();

		// Wait for the modal dialog to close
		lwCreatedHotels.getItems().setAll(ControllerStorage.getHotels());
		for (Hotel hotel : lwHotels.getItems()) {
			if (lwHotels.getItems().contains(hotel)) {
				lwCreatedHotels.getItems().remove(hotel);
			}
		}
	}
	
	private void onDiaHotelDialogCreateAction(HotelCreatePane hotelCreatePane, Stage createHotelDia) {
		if (hotelCreatePane.createHotel()) {
			createHotelDia.hide();
		}
	}

	private void onActionAddHotel() {
		Hotel hotel = lwCreatedHotels.getSelectionModel().getSelectedItem();
		if (hotel != null) {
			lwCreatedHotels.getItems().remove(lwCreatedHotels.getSelectionModel().getSelectedIndex());
			lwHotels.getItems().add(hotel);
		}
	}

	private void onClickCreateConference() {
		if (createConference() == true) {
			ControllerGUI.onClickViewConferences();
		}
	}

	private void onClickCreateAnotherConference() {
		if (createConference() == true) {
			ControllerGUI.onClickMenuCreateConference();
		}
	}
}
