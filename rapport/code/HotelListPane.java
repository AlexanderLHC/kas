package gui;

import java.util.ArrayList;

import application.controller.ControllerStorage;
import application.model.Hotel;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class HotelListPane extends ScrollPane {
	private VBox vbContent = new VBox();

	public HotelListPane() {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		this.getStyleClass().add("hotel-list");

		Label lblHeadline = new Label("Hotels");
		lblHeadline.getStyleClass().add("headline");
		HBox hbHeadline = new HBox(lblHeadline);
		hbHeadline.setAlignment(Pos.CENTER);
		vbContent.getChildren().add(hbHeadline);

		initList();

	}

	private void initList() {
		ArrayList<Hotel> hotels = ControllerStorage.getHotels();
		for (int i = 0; i < hotels.size(); i++) {
			HBox hbHotels = new HBox();
			hbHotels.setAlignment(Pos.CENTER);
			hbHotels.getStyleClass().add("conference-row");

			HotelBox hotelBox = new HotelBox(hotels.get(i));
			if (i % 2 == 1) {
				hbHotels = (HBox) vbContent.getChildren().get(vbContent.getChildren().size() - 1);
			}

			hbHotels.getChildren().add(hotelBox);

			if (i % 2 == 0) {
				vbContent.getChildren().add(hbHotels);
			}
		}

		this.setContent(vbContent);
	}

}
