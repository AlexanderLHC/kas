package gui;

import application.controller.ControllerGUI;
import application.model.Hotel;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class HotelBox extends VBox {

	public HotelBox(Hotel hotel) {
		this.getStyleClass().add("card");
		this.getStyleClass().add("card-hotel");
		
		Label lblTitle = new Label(hotel.getName());
		HBox hbTitle = new HBox(lblTitle);
		hbTitle.getStyleClass().add("headline");
		this.getChildren().add(hbTitle);
		
		Label lblLocation = new Label(hotel.getAddress());
		this.getChildren().add(lblLocation);

		Label lblPricePerDaySingle = new Label(hotel.getPricePerDaySingle() + "");
		this.getChildren().add(lblPricePerDaySingle);

		Label lblPricePerDayDouble = new Label(hotel.getPricePerDayDouble() + "");
		this.getChildren().add(lblPricePerDayDouble);

		//TODO: move action to controller
		this.setOnMouseClicked(event -> ControllerGUI.onActionClickedHotel(event, hotel));
	}

}
