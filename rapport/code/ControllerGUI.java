package application.controller;

import java.time.LocalDate;

import application.model.Conference;
import application.model.Hotel;
import application.model.Registration;
import gui.App;
import gui.ButtonAdd;
import gui.ConferenceCreatePane;
import gui.ConferenceListPane;
import gui.ConferencePane;
import gui.HelpPane;
import gui.HotelCreatePane;
import gui.HotelListPane;
import gui.HotelPane;
import gui.PrintReceiptPane;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class ControllerGUI {
	public static BorderPane pane = App.pane;

	public static void onActionClickedHotel(MouseEvent event, Hotel hotel) {
		openHotelView(hotel);
	}

	public static void openHotelView(Hotel hotel) {
		HotelPane hotelPane = new HotelPane(hotel);
		removeAddButton();
		pane.setCenter(hotelPane);
	}

	public static void openConferenceView(Conference conference) {
		ConferencePane confPane = new ConferencePane(conference);
		removeAddButton();
		pane.setCenter(confPane);
	}

	public static void onActionClickedConference(MouseEvent event, Conference conference) {
		openConferenceView(conference);
	}
	
	public static void onActionClickedPrint(Registration registration) {
		PrintReceiptPane recieptPane = new PrintReceiptPane(registration);
		removeAddButton();
		pane.setCenter(recieptPane);
	}

	public static void onClickViewHotels() {
		HotelListPane hotelList = new HotelListPane();
		cleanScreen();
		pane.lookup(".btn-hotel").setId("active");
		pane.setCenter(hotelList);
		ControllerGUI.addAddButton("addHotel");

		KeyCombination kcC = new KeyCodeCombination(KeyCode.C);
		Runnable rnKeyC = () -> onClickMenuCreateHotel();
		App.getScene().getAccelerators().put(kcC, rnKeyC);
	}

	public static void onClickViewConferences() {
		ConferenceListPane conferenceList = new ConferenceListPane();
		cleanScreen();
		// Hightlights Conference Button
		pane.lookup(".btn-conference").setId("active");
		pane.setCenter(conferenceList);
		ControllerGUI.addAddButton("addConference");

		KeyCombination kcC = new KeyCodeCombination(KeyCode.C);
		Runnable rnKeyC = () -> onClickMenuCreateConference();
		App.getScene().getAccelerators().put(kcC, rnKeyC);
	}

	public static void onClickViewHelp() {
		HelpPane helpPane = new HelpPane();
		cleanScreen();
		// Hightlights Conference Button
		pane.lookup(".btn-help").setId("active");
		pane.setCenter(helpPane);

	}

	public static void onClickDummyData() {
		ControllerStorage.initDummyData();
		onClickViewConferences();
	}

	public static void onClickMenuCreateConference() {
		ConferenceCreatePane conferencePane = new ConferenceCreatePane();
		pane.setCenter(conferencePane);
		removeAddButton();

	}

	public static Conference onClickCreateConference(ConferenceCreatePane confPane, String name, String location, int tickets,
			LocalDate startDate, LocalDate endDate, double pricePerDay) {
		Conference conference = ControllerStorage.createConference(name, tickets, startDate, endDate, pricePerDay, location);
		return conference;

	}

	public static void onClickMenuCreateHotel() {
		HotelCreatePane hotelPane = new HotelCreatePane();
		pane.setCenter(hotelPane);
		removeAddButton();
	}

	public static void addAddButton(String id) {
		Group btnAdd = new ButtonAdd(id, 18);
		btnAdd.getStyleClass().add(id);
		btnAdd.setOnMouseClicked(e -> onClickAddButton(id));

		HBox hbBottom = new HBox(btnAdd);
		hbBottom.setAlignment(Pos.TOP_RIGHT);

		pane.setBottom(hbBottom);
	}

	public static void onClickAddButton(String id) {
		if (id == "addConference") {
			onClickMenuCreateConference();
		} else if (id == "addHotel") {
			onClickMenuCreateHotel();
		}

	}

	public static void cleanScreen() {
		removeHotkeyC();
		removeHotkeyCtrlS();
		removeAddButton();
		clearSelectedNavButton();
	}

	public static void removeHotkeyC() {
		App.getScene().getAccelerators().remove(new KeyCodeCombination(KeyCode.C));
	}
	
	public static void removeHotkeyCtrlS() {
		App.getScene().getAccelerators().remove(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
	}
	

	public static void removeAddButton() {
		pane.setBottom(null);
	}

	public static void clearSelectedNavButton() {
		HBox hbNavButtons = (HBox) pane.lookup(".nav-buttons");

		for (Node button : hbNavButtons.getChildren()) {
			button.setId(null);
		}
	}
}
