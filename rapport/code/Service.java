package application.model;

public class Service {
	private String name;
	private Hotel hotel;
	private double price;

	/**
	 * Creates a new Service object
	 * 
	 * @param name
	 * @param hotel
	 * @param price
	 */

	Service(String name, Hotel hotel, double price) {
		this.name = name;
		this.hotel = hotel;
		this.price = price;
	}

	/**
	 * Returns the hotel of the service
	 * 
	 * @return Hotel hotel of service
	 */

	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * Returns the name of the service
	 * 
	 * @return String name of service
	 */

	public String getName() {
		return name;
	}

	/**
	 * Returns the price of the service
	 * 
	 * @return double price of service
	 */

	public double getPrice() {
		return price;
	}

	/**
	 * Sets the hotel of the service
	 * 
	 * @param hotel new hotel
	 */

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * Sets the name of the service
	 * 
	 * @param name new name
	 */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the price of the service
	 * 
	 * @param price new price
	 */

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return String.format("%s (%.2f kr)", name, price);
	}
}
