package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Registration {
	private Participant participant;
	private Conference conference;
	private int ID;
	private Hotel hotel;
	private String companion;
	private boolean speaker;
	private Company company;
	ArrayList<LocalDate> daysAttending;
	private ArrayList<Service> services = new ArrayList<>();
	private ArrayList<Excursion> excursions = new ArrayList<>();

	Registration(Participant participant, Conference conference, Hotel hotel, String companion, boolean speaker,
			Company company, ArrayList<LocalDate> daysAttending) {
		this.participant = participant;
		this.conference = conference;
		this.hotel = hotel;
		if (hotel != null) {
			hotel.addregistration(this);
		}
		this.companion = companion;
		this.speaker = speaker;
		this.company = company;
		this.daysAttending = daysAttending;
		ID = conference.getParticipantID();
		conference.increaseParticipantID();
	}

	public Participant getParticipant() {
		return participant;
	}

	public Conference getConference() {
		return conference;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Company getCompany() {
		return company;
	}

	public boolean isSpeaker() {
		return speaker;
	}

	public ArrayList<Service> getServices() {
		return new ArrayList<>(services);
	}

	public ArrayList<Excursion> getExcursions() {
		return new ArrayList<>(excursions);
	}

	public ArrayList<LocalDate> getDaysAttending() {
		return daysAttending;
	}

	public void addService(Service service) {
		if (!services.contains(service)) {
			services.add(service);
		}
	}

	public void addExcursion(Excursion excursion) {
		if (!excursions.contains(excursion) && !companion.equals("")) {
			excursions.add(excursion);
			excursion.addAttendee(this);
		}
	}

	public void setExcursion(ArrayList<Excursion> excursions) {
		if (!companion.equals("")) {
			excursions.clear();
			for (Excursion e : excursions) {
				this.excursions.add(e);
			}
		}
	}

	public void removeService(Service service) {
		if (services.contains(service)) {
			services.remove(service);
		}
	}

	public void removeExcursion(Excursion excursion) {
		if (excursions.contains(excursion)) {
			excursions.remove(excursion);
			excursion.removeAttendee(this);
		}
	}

	public double calculatePrice() {

		int duration = daysAttending.size();
		double fullPrice = 0;
		if (!speaker) {
			fullPrice = conference.getPricePerDay() * duration;
		}

		if (hotel != null) {
			int overnights = duration - 1;
			fullPrice += (companion.equals("")) ? hotel.getPricePerDaySingle() * overnights
					: hotel.getPricePerDayDouble() * overnights;
			for (Service s : services) {
				fullPrice += s.getPrice() * overnights;

			}
		}
		if (!companion.equals("")) {
			for (Excursion e : excursions) {
				fullPrice += e.getPrice();
			}
		}

		return fullPrice;
	}

	public void setCompanion(String companion) {
		this.companion = companion;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}

	public void setExcursions(ArrayList<Excursion> excursions) {
		this.excursions = excursions;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public void setServices(ArrayList<Service> services) {
		this.services = services;
	}

	public void setSpeaker(boolean speaker) {
		this.speaker = speaker;
	}

	public int getID() {
		return ID;
	}

	public String getCompanion() {
		return companion;
	}

	@Override
	public String toString() {
		return participant.getName() + " (" + ID + ")";
	}
}
