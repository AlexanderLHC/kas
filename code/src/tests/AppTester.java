package tests;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import application.model.Service;

public class AppTester {
	private Conference havOgHimmel;

	// Hotel
	private Hotel denHvideSvane;
	private Service svaneWifi;
	// Excursions
	private ArrayList<Excursion> excursions = new ArrayList<Excursion>();

	@Before
	public void setUp() {
		// Conference
		havOgHimmel = new Conference("Hav og Himmel", 100, LocalDate.of(2019, 11, 27),
				LocalDate.of(2019, 11, 29), 1500, "Odense");
		// Hotel
		denHvideSvane = new Hotel("Den Hvide Svane", "Andreasen Fork, 9200", 1050, 1250);
		// -services
		svaneWifi = denHvideSvane.createService("WIFI", 50);

		// Excursions
		// - egeskov
		LocalDateTime egeskovDate = LocalDateTime.of(LocalDate.of(2019, 11, 28), LocalTime.of(12, 00));
		Excursion egeskov = new Excursion("Egeskov", "Månen", egeskovDate, 75, false);
		excursions.add(egeskov);
		// - Byrundtur
		LocalDateTime byrundturDate = LocalDateTime.of(LocalDate.of(2019, 11, 27), LocalTime.of(12, 00));
		Excursion byrundtur = new Excursion("Byrundtur", "Odense", byrundturDate, 125, true);
		excursions.add(byrundtur);
		// Trapholt Museum
		LocalDateTime trapholtDate = LocalDateTime.of(LocalDate.of(2019, 11, 29), LocalTime.of(12, 00));
		Excursion trapholt = new Excursion("Trapholt Museum", "Kolding", trapholtDate, 200, true);
		excursions.add(trapholt);

		havOgHimmel.setExcursions(excursions);
	}

	/*
	 * Tests:
	 * 	- [ ] Hotel
	 *  - [ ] Service
	 *  - [ ] Companion
	 *  - [ ] Excursion
	 *  - [3] Days
	 *  - [ ] Speaker
	 */
	@Test
	public void testNoHotelNoServiceNoCompanionNoExcursionAllDaysNotSpeaker() {

		Participant finnMadsen = new Participant("Finn Madsen", "Åboulevarden 32 2nd", 1234678);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));
		daysAttending.add(LocalDate.of(2019, 11, 29));

		Registration registration = havOgHimmel.createRegistration(finnMadsen, null, "", false, null, daysAttending);
		Assert.assertEquals(4500, registration.calculatePrice(), 0);

	}

	/*
	 * Tests:
	 * 	- [x] Hotel
	 *  - [ ] Service
	 *  - [ ] Companion
	 *  - [ ] Excursion
	 *  - [3] Days
	 *  - [ ] Speaker
	 */
	@Test
	public void testYesHotelNoServiceNoCompanionNoExcursionAllDaysNotSpeaker() {

		Participant nielsPetersen = new Participant("Niels Petersen", "Katrine Squares Suite", 1234678);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));
		daysAttending.add(LocalDate.of(2019, 11, 29));

		Registration registration = havOgHimmel.createRegistration(nielsPetersen, denHvideSvane, "", false, null,
				daysAttending);

		Assert.assertEquals(6600, registration.calculatePrice(), 0);
	}

	/*
	 * Tests:
	 * 	- [2 rooms] Hotel
	 *  - [x] Service
	 *  - [x] Companion
	 *  - [x] Excursion
	 *  - [3] Days
	 *  - [ ] Speaker
	 */
	@Test
	public void testYesHotelYesServiceYesCompanionYesExcursionAllDaysNotSpeaker() {
		Participant peterSommer = new Participant("Peter Sommer", "Olfert Island Apt. 704", 1234678);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));
		daysAttending.add(LocalDate.of(2019, 11, 29));

		Registration registration = havOgHimmel.createRegistration(peterSommer, denHvideSvane, "Mie Sommer", false,
				null,
				daysAttending);
		registration.addService(svaneWifi);
		registration.addExcursion(excursions.get(0));
		registration.addExcursion(excursions.get(2));

		Assert.assertEquals(7375, registration.calculatePrice(), 0);
	}

	/*
	 * Tests:
	 * 	- [2 rooms] Hotel
	 *  - [x] Service
	 *  - [x] Companion
	 *  - [x] Excursion
	 *  - [3] Days
	 *  - [x] Speaker
	 */
	@Test
	public void testYesHotelYesServiceYesCompanionYesExcursionAllDaysYesSpeaker() {
		Participant loneJensen = new Participant("Lone Jensen", "Søndergade 12", 88888888);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));
		daysAttending.add(LocalDate.of(2019, 11, 29));

		Registration registration = havOgHimmel.createRegistration(loneJensen, denHvideSvane, "Jan Madsen",
				true, null,
				daysAttending);
		registration.addExcursion(excursions.get(1));
		registration.addExcursion(excursions.get(0));
		registration.addService(svaneWifi);

		Assert.assertEquals(2800, registration.calculatePrice(), 0);
	}

	/*
	 * Tests:
	 * 	- [ ] Hotel
	 *  - [ ] Service
	 *  - [x] Companion
	 *  - [x] Excursion
	 *  - [2] Days
	 *  - [ ] Speaker
	 */
	@Test
	public void testNoHotelNoServiceYesCompanionYesExcursionNotAllDaysNoSpeaker() {
		Participant ullaHansen = new Participant("Ulla Hansen", "Søndergade 12", 1234678);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));

		Registration registration = havOgHimmel.createRegistration(ullaHansen,
				null, "Hans Hansen", false,

				null,
				daysAttending);
		registration.addExcursion(excursions.get(1));

		Assert.assertEquals(3125, registration.calculatePrice(), 0);
	}

	@Test
	public void testIDAfterRemoval() {
		Participant ullaHansen = new Participant("Ulla Hansen", "Søndergade 12", 1234678);

		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));

		Registration registration = havOgHimmel.createRegistration(ullaHansen, null, "Hans Hansen", false, null,
				daysAttending);

		havOgHimmel.removeRegistration(registration);

		Participant ulla = new Participant("Ulla Hansen", "Søndergade 12", 1234678);

		ArrayList<LocalDate> daysAttending1 = new ArrayList<>();
		daysAttending.add(LocalDate.of(2019, 11, 27));
		daysAttending.add(LocalDate.of(2019, 11, 28));

		Registration registration1 = havOgHimmel.createRegistration(ulla, null, "Hans Hansen", false, null,
				daysAttending1);

		Assert.assertEquals(2, registration1.getID(), 0);
	}

}
