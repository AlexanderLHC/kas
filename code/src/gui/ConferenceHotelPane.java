package gui;

import java.util.ArrayList;

import application.model.Conference;
import application.model.Hotel;
import application.model.Registration;
import application.model.Service;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class ConferenceHotelPane extends GridPane {
	private ListView<Hotel> lvwHotels = new ListView<>();
	private VBox bxService = new VBox();

	public ConferenceHotelPane(Conference conference) {
		int j = 0;
		for (int i = 0; i < conference.getHotels().size(); i++) {
			Hotel hotel = conference.getHotels().get(i);

			Label lblHotel = new Label(hotel.getName());
			this.add(lblHotel, 0, j, 3, 1);
			lblHotel.setFont(new Font("Arial", 24));
			j++;

			for (Registration registration : hotel.getRegistrations()) {
				if (registration.getConference() == conference) {
					String guest = registration.getParticipant().getName();
					if (registration.getCompanion() != "") {
						guest += " & " + registration.getCompanion();
					}
					guest += " (" + registration.getParticipant().getPhoneNr() + ")";
					Label lblGuest = new Label(guest);

					String services = "";
					for (Service service : registration.getServices()) {
						services = services + service.getName() + ", ";
					}
					if (services.length() != 0) {
						services = services.substring(0, services.length() - 2);
					} else {
						services = "NO SERVICES SELECTED";
					}
					Label lblServices = new Label(services);

					String daysAttending = registration.getDaysAttending().get(0).toString()
							+ " - "
							+ registration.getDaysAttending().get(registration.getDaysAttending().size() - 1)
									.toString();
					Label lblDaysAttending = new Label(daysAttending);

					VBox vbRegistration = new VBox(lblGuest, lblServices, lblDaysAttending);
					vbRegistration.getStyleClass().add("card");
					vbRegistration.setPadding(new Insets(10));
					vbRegistration.getStyleClass().add("card-registration");
					this.add(vbRegistration, 0, j);
					j++;
				}
			}
			j++;
		}
		;
	}

	public void hotelItemSelected() {
		bxService.getChildren().clear();

		Label lblServices = new Label("Services:");
		bxService.getChildren().add(lblServices);

		Hotel h = lvwHotels.getSelectionModel().getSelectedItem();
		for (Service s : h.getServices()) {
			Label lblService = new Label(s.toString());
			bxService.getChildren().add(lblService);
		}
	}
}