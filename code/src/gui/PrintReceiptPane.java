package gui;

import java.time.LocalDate;
import java.time.LocalTime;

import application.model.Excursion;
import application.model.Registration;
import application.model.Service;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class PrintReceiptPane extends ScrollPane {

	public PrintReceiptPane(Registration r) {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		Label lblHeader = new Label("Printed....");
		lblHeader.setAlignment(Pos.CENTER);
		lblHeader.getStyleClass().add("headline");
		
		// Print
		TextArea taPrinted = new TextArea();
		StringBuilder sbPrint = new StringBuilder();
		
		sbPrint.append("==========RECEIPT==========\n");
		sbPrint.append("\n\n");
		sbPrint.append("\n");
		sbPrint.append("printed: " + LocalDate.now().toString() + "/" + LocalTime.now().toString() + "\n");
		sbPrint.append("reciept id: __________  " + r.getID() + "\n");
		sbPrint.append("conference: ________  " + r.getConference().getName()+ "\n");
		sbPrint.append("location: ___________  " + r.getConference().getLocation()+ "\n");
		sbPrint.append("\n");
		sbPrint.append("name: _______________" + r.getCompanion() + "\n");
		sbPrint.append("\n");
		sbPrint.append("attending day(s): " + "\n" );
		for (LocalDate date : r.getDaysAttending()) {
			sbPrint.append("______ " + date.toString());
		}
		sbPrint.append("\n");
		sbPrint.append("\n");
		if (r.getHotel() != null) {
			sbPrint.append("hotel: " + r.getHotel() + "\n");
			if (r.getServices() != null) {
				for (Service s : r.getServices()) {
					sbPrint.append(s.toString());
				}
			}
		}
		if (r.getCompanion() != null && r.getExcursions() != null) {
			for (Excursion e : r.getExcursions() ) {
				sbPrint.append("______ " + e.toString() + "\n");
			}
		}
		sbPrint.append("\n");
		sbPrint.append("total price_ _ _ _ _ _ _ _ _ _ _ _ _" + r.calculatePrice() + " kr");
		sbPrint.append("\n");
		sbPrint.append("\n");
		sbPrint.append("contact: " + r.getConference().getName().replaceAll("\\s+","") + "@conference.com");

		taPrinted.setText(sbPrint.toString());

		HBox hbCenter = new HBox(taPrinted);
		hbCenter.setAlignment(Pos.CENTER);

		this.setContent(hbCenter);
	}
}
