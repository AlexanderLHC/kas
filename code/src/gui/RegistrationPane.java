package gui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import application.controller.ControllerStorage;

import org.controlsfx.control.CheckComboBox;

import application.model.Company;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import application.model.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RegistrationPane extends GridPane {
	private DatePicker datePickerStart, datePickerEnd;
	private RadioButton rbnYesSpeaker, rbnNoSpeaker, rbnYesCompanion, rbnNoCompanion;
	private ComboBox cmbHotels;
	private CheckComboBox<Excursion> ccbExcursions;
	private CheckComboBox<Service> ccbServices;
	private Label lblServices, lblCompanionName, lblExcursions;
	private final ToggleGroup tggSpeaker = new ToggleGroup();
	private final ToggleGroup tggCompanion = new ToggleGroup();
	private TextField txfName, txfAddress, txfCityCountry, txfPhoneNumber, txfCompanyName, txfCompanyPhone,
			txfCompanionName;
	private Button btnCreate;
	private Conference conference;

	public RegistrationPane(Conference conference) {
		this.conference = conference;

		Label lblName = new Label("Name of participant:");
		this.add(lblName, 0, 0);
		lblName.setPadding(new Insets(5));

		txfName = new TextField();
		this.add(txfName, 1, 0);

		Label lblAddress = new Label("Address:");
		this.add(lblAddress, 2, 0);
		lblAddress.setPadding(new Insets(5));

		txfAddress = new TextField();
		this.add(txfAddress, 3, 0);

		Label lblCityCountry = new Label("City/Country:");
		this.add(lblCityCountry, 0, 1);
		lblCityCountry.setPadding(new Insets(5));

		txfCityCountry = new TextField();
		this.add(txfCityCountry, 1, 1);

		Label lblArrivalDate = new Label("Date of arrival:");
		this.add(lblArrivalDate, 0, 2);
		lblArrivalDate.setPadding(new Insets(5));

		datePickerStart = new DatePicker();
		datePickerStart.setValue(conference.getStartDate());
		this.add(datePickerStart, 1, 2);
		datePickerStart.setDayCellFactory(picker -> new DateCell() {
			@Override
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate startDate = conference.getStartDate();
				LocalDate endDate = conference.getEndDate();
				setDisable(empty || date.compareTo(startDate) < 0 || date.compareTo(endDate) > 0);

				if (datePickerEnd.getValue() != null) {
					if (datePickerStart.getValue() != null) {
						if (datePickerStart.getValue().compareTo(datePickerEnd.getValue()) > 0) {
							datePickerEnd.setValue(null);
						}
					}
				}
			}
		});

		Label lblDepartureDate = new Label("Date of departure");
		this.add(lblDepartureDate, 2, 2);
		lblDepartureDate.setPadding(new Insets(5));

		datePickerEnd = new DatePicker();
		datePickerEnd.setValue(conference.getEndDate());
		this.add(datePickerEnd, 3, 2);
		datePickerEnd.setDayCellFactory(picker -> new DateCell() {
			@Override
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate startDate = conference.getStartDate();
				if (datePickerStart.getValue() != null) {
					if (datePickerStart.getValue().compareTo(startDate) > 0) {
						startDate = datePickerStart.getValue();
					}
				}
				LocalDate endDate = conference.getEndDate();
				setDisable(empty || date.compareTo(startDate) < 0 || date.compareTo(endDate) > 0);
			}
		});

		Label lblPhoneNumber = new Label("Phone number:");
		this.add(lblPhoneNumber, 2, 1);
		lblPhoneNumber.setPadding(new Insets(5));

		txfPhoneNumber = new TextField();
		this.add(txfPhoneNumber, 3, 1);

		Label lblCompanyName = new Label("Company name:");
		this.add(lblCompanyName, 0, 3);
		lblCompanyName.setPadding(new Insets(5));

		txfCompanyName = new TextField();
		this.add(txfCompanyName, 1, 3);

		Label lblCompanyPhone = new Label("Company phone number:");
		this.add(lblCompanyPhone, 2, 3);
		lblCompanyPhone.setPadding(new Insets(5));

		txfCompanyPhone = new TextField();
		this.add(txfCompanyPhone, 3, 3);

		Label lblSpeaker = new Label("Speaker:");
		this.add(lblSpeaker, 0, 4);
		lblSpeaker.setPadding(new Insets(5));

		HBox hbxSpeakerRadioButtons = new HBox();
		this.add(hbxSpeakerRadioButtons, 1, 4);

		rbnYesSpeaker = new RadioButton();
		hbxSpeakerRadioButtons.getChildren().add(rbnYesSpeaker);
		rbnYesSpeaker.setText("Yes");
		rbnYesSpeaker.setToggleGroup(tggSpeaker);
		rbnYesSpeaker.setPadding(new Insets(5));

		rbnNoSpeaker = new RadioButton();
		hbxSpeakerRadioButtons.getChildren().add(rbnNoSpeaker);
		rbnNoSpeaker.setText("No");
		rbnNoSpeaker.setToggleGroup(tggSpeaker);
		rbnNoSpeaker.setPadding(new Insets(5));
		rbnNoSpeaker.setSelected(true);

		Label lblCompanion = new Label("Companion:");
		this.add(lblCompanion, 2, 4);
		lblCompanion.setPadding(new Insets(5));

		HBox hbxCompanionRadioButtons = new HBox();
		this.add(hbxCompanionRadioButtons, 3, 4);

		rbnYesCompanion = new RadioButton();
		hbxCompanionRadioButtons.getChildren().add(rbnYesCompanion);
		rbnYesCompanion.setText("Yes");
		rbnYesCompanion.setToggleGroup(tggCompanion);
		rbnYesCompanion.setPadding(new Insets(5));
		rbnYesCompanion.setOnAction(event -> onCompanionYesSelected());

		rbnNoCompanion = new RadioButton();
		hbxCompanionRadioButtons.getChildren().add(rbnNoCompanion);
		rbnNoCompanion.setText("No");
		rbnNoCompanion.setToggleGroup(tggCompanion);
		rbnNoCompanion.setPadding(new Insets(5));
		rbnNoCompanion.setSelected(true);
		rbnNoCompanion.setOnAction(event -> onCompanionNoSelected());

		lblCompanionName = new Label("Name of companion:");
		this.add(lblCompanionName, 2, 5);
		lblCompanionName.setVisible(false);
		lblCompanionName.setPadding(new Insets(5));

		txfCompanionName = new TextField();
		this.add(txfCompanionName, 3, 5);
		txfCompanionName.setVisible(false);

		lblExcursions = new Label("Excursions:");
		this.add(lblExcursions, 2, 6);
		lblExcursions.setVisible(false);
		lblExcursions.setPadding(new Insets(5));

		ObservableList<Excursion> excursions = FXCollections.observableArrayList(conference.getExcursions());
		ccbExcursions = new CheckComboBox<>(excursions);
		this.add(ccbExcursions, 3, 6);
		ccbExcursions.setVisible(false);
		ccbExcursions.setMaxWidth(150);

		Label lblHotel = new Label("Hotel:");
		this.add(lblHotel, 0, 5);
		lblHotel.setPadding(new Insets(5));

		cmbHotels = new ComboBox(FXCollections.observableArrayList(conference.getHotels()));
		cmbHotels.getItems().add("None");
		this.add(cmbHotels, 1, 5);
		cmbHotels.setOnAction(event -> onHotelChange());

		lblServices = new Label("Select services:");
		this.add(lblServices, 0, 6);
		lblServices.setPadding(new Insets(5));
		lblServices.setVisible(false);

		ccbServices = new CheckComboBox<>();
		this.add(ccbServices, 1, 6);
		ccbServices.setVisible(false);
		ccbServices.setMaxWidth(150);

		btnCreate = new Button();
		btnCreate.setText("Create registration");
		this.add(btnCreate, 0, 9);
		btnCreate.setPadding(new Insets(5));
		btnCreate.setOnAction(event -> createRegistrationAction());
	}

	private void onHotelChange() {
		if (cmbHotels.getSelectionModel().getSelectedItem() != null
				& cmbHotels.getSelectionModel().getSelectedItem() != "None") {
			ccbServices.setVisible(true);
			lblServices.setVisible(true);

			Hotel hotel = (Hotel) cmbHotels.getSelectionModel().getSelectedItem();
			ObservableList<Service> services = FXCollections.observableArrayList(hotel.getServices());
			ccbServices.getItems().setAll(services);

		} else {
			ccbServices.setVisible(false);
			lblServices.setVisible(false);
		}
	}

	private void onCompanionYesSelected() {
		lblCompanionName.setVisible(true);
		lblExcursions.setVisible(true);
		ccbExcursions.setVisible(true);
		txfCompanionName.setVisible(true);
	}

	private void onCompanionNoSelected() {
		lblCompanionName.setVisible(false);
		lblExcursions.setVisible(false);
		ccbExcursions.setVisible(false);
		txfCompanionName.setVisible(false);
	}

	private void createRegistrationAction() {
		StringBuilder errorString = new StringBuilder();

		String name = txfName.getText().trim();
		if (name.length() == 0) {
			errorString.append("No name entered.\n");
		}

		int number = -1;
		try {
			number = Integer.parseInt(txfPhoneNumber.getText().trim());
			if (number < 10000000 || number > 99999999) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			errorString.append("Phone number entered incorrectly. Please input an 8 digit number\n");
		}

		String cityCountry = txfCityCountry.getText().trim();
		if (cityCountry.length() == 0) {
			errorString.append("No city or country entered.\n");
		}

		String address = txfAddress.getText().trim();
		if (address.length() == 0) {
			errorString.append("No address entered.\n");
		}

		LocalDate arrivalDate = datePickerStart.getValue();
		LocalDate departureDate = datePickerEnd.getValue();
		ArrayList<LocalDate> daysAttending = new ArrayList<>();
		if (arrivalDate != null && departureDate != null) {
			long numOfDaysBetween = ChronoUnit.DAYS.between(arrivalDate, departureDate);
			daysAttending = (ArrayList<LocalDate>) IntStream.iterate(0, i -> i + 1)
					.limit(numOfDaysBetween)
					.mapToObj(i -> arrivalDate.plusDays(i))
					.collect(Collectors.toList());
		} else {
			errorString.append("No arrival or departure date selected.\n");
		}

		String companyName = txfCompanyName.getText().trim();
		int companyPhone = -1;
		Company company = null;

		if (companyName.length() != 0 && txfCompanyPhone.getText().trim().length() == 0) {
			errorString.append("No company phone number entered.\n");
		} else if (companyName.length() == 0 && txfCompanyPhone.getText().trim().length() != 0) {
			errorString.append("No company name entered.\n");
		} else if (companyName.length() != 0 && txfCompanyPhone.getText().trim().length() != 0) {
			try {
				companyPhone = Integer.parseInt(txfCompanyPhone.getText().trim());
				if (companyPhone < 10000000 || companyPhone > 99999999) {
					throw new NumberFormatException();
				}
				company = ControllerStorage.createCompany(companyName, companyPhone);
			} catch (NumberFormatException e) {
				errorString.append("Company phone number entered incorrectly. Please input an 8 digit number\n");
			}
		}

		boolean speaker = rbnYesSpeaker.isSelected() ? true : false;

		ArrayList<Excursion> excursions = new ArrayList<>();
		String companion = "";
		if (rbnYesCompanion.isSelected()) {
			companion = txfCompanionName.getText().trim();
			if (companion.length() == 0) {
				errorString.append("No companion name entered.\n");
			}
			excursions = new ArrayList<>(ccbExcursions.getCheckModel().getCheckedItems());
		}

		Hotel hotel = null;
		ArrayList<Service> services = new ArrayList<>();
		if (cmbHotels.getSelectionModel().getSelectedItem() != null
				& cmbHotels.getSelectionModel().getSelectedItem() != "None") {
			hotel = (Hotel) cmbHotels.getSelectionModel().getSelectedItem();
			services = new ArrayList<>(ccbServices.getCheckModel().getCheckedItems());
		}

		if (errorString.length() == 0) {
			Participant participant = ControllerStorage.createParticipant(name, address, number);
			Registration registration = ControllerStorage.createRegistration(participant, conference, hotel, companion,
					speaker, company, daysAttending);
			for (Excursion e : excursions) {
				registration.addExcursion(e);
			}
			for (Service s : services) {
				registration.addService(s);
			}
			Alert successAlert = new Alert(AlertType.INFORMATION);
			successAlert.setTitle("Success");
			successAlert.setHeaderText("Registration created succesfully.");
			successAlert.show();

			txfAddress.clear();
			txfCityCountry.clear();
			txfCompanionName.clear();
			txfCompanyName.clear();
			txfCompanyPhone.clear();
			txfName.clear();
			txfPhoneNumber.clear();
			cmbHotels.setValue(null);
			rbnNoSpeaker.setSelected(true);
			rbnNoCompanion.setSelected(true);
			this.onCompanionNoSelected();
			this.onHotelChange();
		} else {
			Alert errorAlert = new Alert(AlertType.ERROR);
			errorAlert.setTitle("Error");
			errorAlert.setHeaderText("An error occured. Please see below.");
			errorAlert.setContentText(errorString.toString());
			errorAlert.show();
		}
	}
}
