package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import application.controller.ControllerStorage;
import application.model.Conference;
import application.model.Excursion;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ExcursionCreateWindow extends Stage {
	private Conference conference;

	public ExcursionCreateWindow(String title, Conference conference) {
		this.conference = conference;

		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

	}

	private TextField txfName, txfLocation, txfPrice, txfStartHour;
	private DatePicker datePicker;
	private final ToggleGroup group = new ToggleGroup();
	private RadioButton rbnYes, rbnNo;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblName = new Label("Name:");
		pane.add(lblName, 0, 0);

		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblLocation = new Label("Location:");
		pane.add(lblLocation, 0, 1);

		txfLocation = new TextField();
		pane.add(txfLocation, 1, 1);

		Label lblDate = new Label("Date:");
		pane.add(lblDate, 0, 2);

		datePicker = new DatePicker();
		datePicker.setValue(conference.getStartDate());
		pane.add(datePicker, 1, 2);
		datePicker.setDayCellFactory(picker -> new DateCell() {
			@Override
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate startDate = conference.getStartDate();
				LocalDate endDate = conference.getEndDate();
				setDisable(empty || date.compareTo(startDate) < 0 || date.compareTo(endDate) > 0);
			}
		});

		Label lblStartHour = new Label("Start time (hour):");
		pane.add(lblStartHour, 0, 3);

		txfStartHour = new TextField();
		pane.add(txfStartHour, 1, 3);

		Label lblPrice = new Label("Price:");
		pane.add(lblPrice, 0, 4);

		txfPrice = new TextField();
		pane.add(txfPrice, 1, 4);

		Label lblLunch = new Label("Lunch:");
		pane.add(lblLunch, 0, 5);

		HBox hbxRadioButtons = new HBox();
		pane.add(hbxRadioButtons, 1, 5);

		rbnYes = new RadioButton();
		hbxRadioButtons.getChildren().add(rbnYes);
		rbnYes.setText("Yes");
		rbnYes.setToggleGroup(group);
		rbnYes.setPadding(new Insets(5));

		rbnNo = new RadioButton();
		hbxRadioButtons.getChildren().add(rbnNo);
		rbnNo.setText("No");
		rbnNo.setToggleGroup(group);
		rbnNo.setPadding(new Insets(5));
		rbnNo.setSelected(true);

		HBox hbxButtons = new HBox();
		pane.add(hbxButtons, 0, 6, 3, 1);
		hbxButtons.setPadding(new Insets(10, 0, 0, 0));
		hbxButtons.setAlignment(Pos.BASELINE_CENTER);

		Button btnCreate = new Button("Create");
		hbxButtons.getChildren().add(btnCreate);
		btnCreate.setOnAction(event -> createAction());

		Button btnCancel = new Button("Cancel");
		hbxButtons.getChildren().add(btnCancel);
		btnCancel.setOnAction(event -> cancelAction());
	}

	private void createAction() {
		StringBuilder errorString = new StringBuilder();

		String name = txfName.getText().trim();
		if (name.length() == 0) {
			errorString.append("No name entered.\n");
		}

		String location = txfLocation.getText().trim();
		if (location.length() == 0) {
			errorString.append("No location entered.\n");
		}

		LocalDate date = datePicker.getValue();

		String startHourInText = txfStartHour.getText().trim();
		int startHour = -1;
		if (startHourInText.length() == 0) {
			errorString.append("No start hour entered.\n");
		} else {
			try {
				startHour = Integer.parseInt(startHourInText);
				if (startHour < 0 || startHour > 23) {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException e) {
				errorString.append("Start hour entered incorrectly. Please enter an integer between 0 - 23.\n");
			}
		}

		double price = -1;
		try {
			price = Double.parseDouble(txfPrice.getText().trim());
			if (price < 0) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			errorString.append("Invalid price entered.\n");
		}

		boolean lunch = rbnYes.isSelected() ? true : false;

		if (errorString.toString().length() == 0) {
			LocalTime time = LocalTime.of(startHour, 0);
			LocalDateTime dateAndTime = LocalDateTime.of(date, time);
			Excursion excursion = ControllerStorage.createExcursion(name, location, dateAndTime, price, lunch);
			ControllerStorage.addExcursionToConference(conference, excursion);

			Alert successAlert = new Alert(AlertType.INFORMATION);
			successAlert.setTitle("Success");
			successAlert.setHeaderText("Excursion created succesfully.");
			successAlert.show();

			this.hide();
		} else {
			Alert errorAlert = new Alert(AlertType.ERROR);
			errorAlert.setTitle("Error");
			errorAlert.setHeaderText("An error occured. Please see below.");
			errorAlert.setContentText(errorString.toString());
			errorAlert.show();
		}

	}

	private void cancelAction() {
		this.hide();
	}
}
