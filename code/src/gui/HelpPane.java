package gui;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HelpPane extends ScrollPane {

	public HelpPane() {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		this.getStylesheets().add(getClass().getResource("ConferenceHotelPane.css").toExternalForm());
		Label lblHeader = new Label("Help!");
		lblHeader.setAlignment(Pos.CENTER);
		lblHeader.getStyleClass().add("headline");
		
		// Hotkeys
		Label lblHotkeys = new Label("Hotkeys");
		lblHotkeys.getStyleClass().add("headline");
		Label lblNavHead = new Label("Navigation");
		lblHotkeys.getStyleClass().add("headline-sub");
		Label lblHotkeyF1 = new Label(String.format("[F1] %-10s", "Conference"));
		Label lblHotkeyF2 = new Label(String.format("[F2] %-10s", "Hotels"));
		Label lblHotkeyF3 = new Label(String.format("[F3] %-10s", "Help (this page)"));
		VBox vbNavHotkeys = new VBox(lblHotkeyF1, lblHotkeyF2, lblHotkeyF3);
		Label lblOther = new Label("Other");
		Label lblHotkeyC = new Label(String.format("[C] %-10s", "Create when + button is visible."));
		Label lblHotkeyCtrlS = new Label(String.format("[Ctrl+S] %-10s", "Saves current conferencce or hotel."));
		VBox vbNavOthers = new VBox(lblHotkeyC,lblHotkeyCtrlS);
		vbNavHotkeys.getStyleClass().add("hotkeys");
		vbNavOthers.getStyleClass().add("hotkeys");
		VBox vbNavigation = new VBox(lblNavHead, vbNavHotkeys);
		VBox vbOther = new VBox(lblOther, vbNavOthers);
		VBox vbHotkeys = new VBox(lblHotkeys, vbNavigation, vbOther);
		VBox vbContent = new VBox(lblHeader, vbHotkeys);
		vbContent.getStyleClass().add("card");
		vbContent.getStyleClass().add("card-help");
		
		// Buttons
		Label lblButtons = new Label("Buttons");
		lblButtons.getStyleClass().add("headline");

		Button btnConf = new Button("");
		Label lblConference = new Label("show list of conferences.");
		btnConf.getStyleClass().add("btn-conference");
		HBox hbConference = new HBox(btnConf, lblConference);

		Button btnHotel = new Button("");
		btnHotel.getStyleClass().add("btn-hotel");	
		Label lblHotel = new Label("show list of hotels.");
		HBox hbHotel = new HBox(btnHotel, lblHotel);
		
		
		Button btnDummy = new Button("");
		btnDummy.getStyleClass().add("btn-dummy");	
		Label lblDummy = new Label("adds dummy content for debugging.");
		HBox hbDummy = new HBox(btnDummy, lblDummy);

		Button btnDelete = new Button("");
		btnDelete.getStyleClass().add("btn-delete");
		Label lblDelete = new Label("deletes currrent selected item.");
		HBox hbDelete = new HBox(btnDelete, lblDelete);
		
		
		Button btnEdit = new Button("");
		btnEdit.setId("edit");
		Label lblEdit = new Label("edits current selected item.");
		HBox hbEdit = new HBox(btnEdit, lblEdit);

		Button btnSave = new Button("");
		btnSave.setId("save");
		Label lblSave = new Label("saves the changes.");
		HBox hbSave = new HBox(btnSave, lblSave);

		
		vbContent.getChildren().addAll(lblButtons, hbConference, hbHotel, hbDummy, hbDelete, hbEdit, hbSave);
		
		HBox hbCenter = new HBox(vbContent);
		hbCenter.setAlignment(Pos.CENTER);

		this.setContent(hbCenter);
	}
}
