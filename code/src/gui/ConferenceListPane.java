package gui;

import java.util.ArrayList;

import application.controller.ControllerStorage;
import application.model.Conference;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ConferenceListPane extends ScrollPane {
	private VBox vbContent = new VBox();

	public ConferenceListPane() {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		this.getStyleClass().add("conference-list");

		Label lblHeadline = new Label("Conferences");
		lblHeadline.getStyleClass().add("headline");
		HBox hbHeadline = new HBox(lblHeadline);
		hbHeadline.setAlignment(Pos.CENTER);
		vbContent.getChildren().add(hbHeadline);
		initList();
	}

	private void initList() {
		ArrayList<Conference> conferences = ControllerStorage.getConferences();
		for (int i = 0; i < conferences.size(); i++) {
			HBox hbConferences = new HBox();
			hbConferences.setAlignment(Pos.CENTER);
			hbConferences.getStyleClass().add("conference-row");

			ConferenceBox confBox = new ConferenceBox(conferences.get(i));
			if (i % 2 == 1) {
				hbConferences = (HBox) vbContent.getChildren().get(vbContent.getChildren().size() - 1);
			}

			hbConferences.getChildren().add(confBox);

			if (i % 2 == 0) {
				vbContent.getChildren().add(hbConferences);
			}
		}
		this.setContent(vbContent);
	}
}
