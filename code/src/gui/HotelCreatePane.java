package gui;

import java.util.HashMap;

import application.controller.ControllerGUI;
import application.controller.ControllerStorage;
import application.model.Hotel;
import application.model.Service;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HotelCreatePane extends ScrollPane {
	private TextField txfName, txfAddress, txfPricePerDaySingle, txfPricePerDayDouble, txfServiceName, txfServicePrice;
	private Label lblNameError, lblAddressError, lblPricePerDaySingleError, lblPricePerDayDoubleError;
	private ListView<String> lvwServices = new ListView<>();
	private HashMap<String, Double> mapServiceNameAndPrice = new HashMap<>();
	private Text txError;
	private GridPane gpContentPane = new GridPane();

	public HotelCreatePane() {
		App.getScene().getStylesheets().add(getClass().getResource("Card.css").toExternalForm());
		
		gpContentPane.getStylesheets().add(getClass().getResource("ConferenceHotelPane.css").toExternalForm());
		gpContentPane.getStyleClass().add("card");
		gpContentPane.getStyleClass().add("card-hotel");

		// Save Hotkey
		KeyCombination kcCtrlS = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
		Runnable rnKeyCtrlS = () -> onClickCreateHotel();
		App.getScene().getAccelerators().put(kcCtrlS, rnKeyCtrlS);
		
		initContent();
	}

	public void initContent() {

		Label lblName = new Label("Name: ");
		txfName = new TextField();
		lblNameError = new Label("");
		gpContentPane.add(lblName, 0, 0);
		gpContentPane.add(txfName, 1, 0);
		gpContentPane.add(lblNameError, 2, 0);


		Label lblAddress = new Label("Address: ");
		txfAddress = new TextField();
		lblAddressError = new Label("");
		gpContentPane.add(lblAddress, 0, 1);
		gpContentPane.add(txfAddress, 1, 1);
		gpContentPane.add(lblAddressError, 2, 1);
		
		Label lblPricePerDaySingle = new Label("Price per day singles room: ");
		txfPricePerDaySingle = new TextField();
		lblPricePerDaySingleError = new Label("");
		gpContentPane.add(lblPricePerDaySingle, 0, 2);
		gpContentPane.add(txfPricePerDaySingle, 1, 2);
		gpContentPane.add(lblPricePerDaySingleError, 2, 2);

		Label lblPricePerDayDouble = new Label("Price per day doubles room: ");
		txfPricePerDayDouble = new TextField();
		lblPricePerDayDoubleError = new Label("");
		gpContentPane.add(lblPricePerDayDouble, 0, 3);
		gpContentPane.add(txfPricePerDayDouble, 1, 3);
		gpContentPane.add(lblPricePerDayDoubleError, 2, 3);

		
		Label lblServices = new Label("Services");
		gpContentPane.add(lblServices, 0, 4, 2 ,3);
		gpContentPane.add(lvwServices, 0, 5, 2 ,3);

		Button btnAddService = new Button("Add service");
		btnAddService.setOnAction(e -> onActionAddService());
		Button btnDeleteService = new Button("Delete service");
		btnDeleteService.setOnAction(e -> onActionRemoveService());
		HBox hbServiceBtns = new HBox(btnAddService, btnDeleteService);
		
		gpContentPane.add(hbServiceBtns, 0, 8, 3, 1);
		Label lblServiceName = new Label("Name:");
		txfServiceName = new TextField("");
		gpContentPane.add(lblServiceName, 0, 9);
		gpContentPane.add(txfServiceName, 1, 9);

		Label lblServicePrice = new Label("Price:");
		txfServicePrice = new TextField("");
		gpContentPane.add(lblServicePrice, 0, 10);
		gpContentPane.add(txfServicePrice, 1, 10);
		
		txError = new Text("");
		HBox hbError = new HBox(txError);
		hbError.getStyleClass().add("text-error");
		gpContentPane.add(hbError, 0, 11);

		// Buttons
		HBox hbButtons = new HBox();
		Button btnAdd = new Button("Add");
		btnAdd.setId("add");
		btnAdd.setOnAction(e -> this.onClickCreateHotel());
		Button btnAddMore = new Button("Add more");
		btnAddMore.setId("addMore");
		btnAddMore.setOnAction(e -> this.onClickCreateAnotherHotel());
		Button btnCancel = new Button("Cancel");
		btnCancel.setId("cancel");
		btnCancel.setOnAction(e -> ControllerGUI.onClickViewHotels());
		hbButtons.getChildren().addAll(btnAdd, btnAddMore, btnCancel);
		gpContentPane.add(hbButtons, 0, 12, 3, 1);

		HBox hbCenter = new HBox(gpContentPane);
		hbCenter.setAlignment(Pos.CENTER);
		
		this.setContent(hbCenter);
	}

	private void onActionAddService() {
		String name = txfServiceName.getText();
		String priceString = txfServicePrice.getText().trim();
		StringBuilder sbError = new StringBuilder();
		boolean hasError = false;

		if (name.isEmpty()) {
			sbError.append("* name: can't be empty.\n");
			hasError = true;
		}
		if (priceString.isEmpty()
				|| !priceString.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* price: can't be empty and only integers.\n");
			hasError = true;
		}

		if (!hasError) {
			Double price = Double.parseDouble(priceString);

			mapServiceNameAndPrice.put(name, price);
			lvwServices.getItems().add(name + " (" + price + ")");
			txfServiceName.setText("");
			txfServicePrice.setText("");
			txError.setText("");
		} else {
			txError.setText(sbError.toString());
		}
	}

	private void onActionRemoveService() {
		String service = lvwServices.getSelectionModel().getSelectedItem();
		lvwServices.getItems().remove(service);
		mapServiceNameAndPrice.remove(service.split("\\s+")[0]);
	}

	public boolean createHotel() {
		boolean hasError = false;
		String name = txfName.getText();
		String address = txfAddress.getText();
		String pricePerDaySingle = txfPricePerDaySingle.getText();
		String pricePerDayDouble = txfPricePerDayDouble.getText();
		StringBuilder sbError = new StringBuilder();

		if (name.isEmpty() || name.length() < 3) {
			sbError.append("* name can't be empty or less than 3 characters.\n");
			hasError = true;
		}
		if (address.isEmpty()) {
			sbError.append("* address: can't be empty.\n");
			hasError = true;
		}
		if (pricePerDaySingle.isEmpty()
				|| !pricePerDaySingle.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* price per day single: can't be empty and only integers.\n");
			hasError = true;
		}
		if (pricePerDayDouble.isEmpty()
				|| !pricePerDayDouble.matches("(?<=^| )\\d+(\\.\\d+)?(?=$| )|(?<=^| )\\.\\d+(?=$| )")) {
			sbError.append("* price per day double: can't be empty and only integers.\n");
			hasError = true;
		}
		if (!hasError) {
			Hotel hotel = ControllerStorage.createHotel(name, address, Double.parseDouble(pricePerDaySingle),
					Double.parseDouble(pricePerDayDouble));
			for (String s : lvwServices.getItems()) {
				double price = mapServiceNameAndPrice.get(s.split("\\s+")[0]);
				Service service = ControllerStorage.createService(s, hotel, price);
			}
		} else {
			txError.setText(sbError.toString());
		}
		return !hasError;
	}

	private void onClickCreateHotel() {
		if (createHotel() == true) {
			ControllerGUI.onClickViewHotels();
		}
	}

	private void onClickCreateAnotherHotel() {
		if (createHotel() == true) {
			ControllerGUI.onClickMenuCreateHotel();
		}
	}
}
