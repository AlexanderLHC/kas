package gui;

import application.controller.ControllerGUI;
import application.model.Conference;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ConferenceBox extends VBox {
	public ConferenceBox(Conference conference) {
		this.getStyleClass().add("card");
		this.getStyleClass().add("card-conference");

		Label lblName = new Label(conference.getName());
		HBox hbName = new HBox(lblName);
		hbName.getStyleClass().add("headline");
		this.getChildren().add(hbName);

		int seedText = conference.getName().charAt(0) + conference.getName().charAt(1) + conference.getName().charAt(2);
		Image coverImg = new Image("https://picsum.photos/seed/" + seedText + "/300/300");
		HBox hbCover = new HBox(new ImageView(coverImg));
		hbCover.getStyleClass().add("cover-image");
		this.getChildren().add(hbCover);

		VBox vbContent = new VBox();
		GridPane gpContent = new GridPane();

		gpContent.add(new Label("Addresse:"), 0, 0);
		gpContent.add(new Label(conference.getLocation()), 1, 0);

		gpContent.add(new Label("Billetter:"), 0, 1);
		gpContent.add(new Label(conference.getTicketsSold() + "/" + conference.getTickets()), 1, 1);

		gpContent.add(new Label("Datoer:"), 0, 2);
		gpContent.add(new Label(conference.getStartDate().toString() + " - " + conference.getEndDate().toString()), 1,
				2);

		this.getChildren().add(gpContent);
		vbContent.getStyleClass().add("content");

		this.setOnMouseClicked(event -> ControllerGUI.onActionClickedConference(event, conference));
	}
}
