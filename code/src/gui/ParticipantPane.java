package gui;

import org.controlsfx.control.CheckComboBox;

import application.controller.ControllerGUI;
import application.controller.ControllerStorage;
import application.model.Company;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import application.model.Service;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import storage.Storage;

public class ParticipantPane extends GridPane {

	private ListView<Registration> lvwRegistrations = new ListView<>();
	private ListView<Service> lvwServices = new ListView<>();
	private ListView<Excursion> lvwExcursions = new ListView<>();
	private GridPane infoPane = new GridPane();
	private TextField txfName, txfID, txfPhoneNr, txfAddress, txfCompany, txfCompanyPhoneNr, txfCompanion, txfHotel,
			txfPrice;
	private Button btnEdit, btnDelete, btnPrint;
	private HBox btnBox;
	private CheckBox cbxSpeaker = new CheckBox();
	private DatePicker datePickerStart, datePickerEnd;
	private Conference conference;
	private CheckComboBox<Excursion> ccbExcursions;
	private CheckComboBox<Service> ccbServices;
	private ComboBox cmbHotels, cmbCompany;
	private Hotel chosenHotel;

	public ParticipantPane(Conference conference) {
		this.getStyleClass().add("participant-pane");
		this.conference = conference;

		this.setHeight(1000.0);
		this.add(lvwRegistrations, 0, 0, 1, 1);
		this.add(infoPane, 1, 0);

		lvwRegistrations.getItems().setAll(conference.getRegistrations());

		ChangeListener<Registration> listener = (ov, o, n) -> this.participantItemSelected(conference);
		lvwRegistrations.getSelectionModel().selectedItemProperty().addListener(listener);

		btnBox = new HBox();
		this.add(btnBox, 0, 1);

		btnEdit = new Button("");
		btnEdit.setId("edit");
		btnBox.getChildren().add(0, btnEdit);
		btnEdit.setOnAction(event -> editAction(conference));

		btnDelete = new Button("");
		btnDelete.getStyleClass().add("btn-delete");
		btnBox.getChildren().add(1, btnDelete);
		btnDelete.setOnAction(event -> deleteAction(conference));
		
		btnPrint = new Button("");
		btnPrint.getStyleClass().add("btn-print");
		btnBox.getChildren().add(2, btnPrint);
		btnPrint.setOnAction(event -> printActionAction());

	}

	private void participantItemSelected(Conference conference) {
		infoPane.getChildren().clear();
		btnEdit.setId("edit");

		Registration r = lvwRegistrations.getSelectionModel().getSelectedItem();

		if (conference.getRegistrations().contains(r)) {

			Label lblName = new Label("Name:");
			infoPane.add(lblName, 0, 0);

			cbxSpeaker = new CheckBox("Speaker");
			cbxSpeaker.setDisable(true);
			infoPane.add(cbxSpeaker, 2, 0);
			if (r.isSpeaker()) {
				cbxSpeaker.setSelected(true);
			}

			Label lblID = new Label("ID:");
			infoPane.add(lblID, 0, 1);

			Label lblPPhoneNr = new Label("Phone number:");
			infoPane.add(lblPPhoneNr, 0, 2);

			Label lblPAddress = new Label("Address:");
			infoPane.add(lblPAddress, 0, 3);

			Label lblCompany = new Label("Company:");
			infoPane.add(lblCompany, 0, 4);

			Label lblCompanyPhoneNr = new Label("Company phone:");
			infoPane.add(lblCompanyPhoneNr, 2, 3);

			Label lblCompanion = new Label("Companion:");
			infoPane.add(lblCompanion, 0, 5);

			Label lblDaysAttending = new Label("Days attending");
			infoPane.add(lblDaysAttending, 0, 6);

			Label lblHotel = new Label("Hotel:");
			infoPane.add(lblHotel, 0, 7);

			Label lblServices = new Label("Services:");
			infoPane.add(lblServices, 0, 8);

			Label lblPrice = new Label("Price:");
			infoPane.add(lblPrice, 0, 9);

			Label lblExcursions = new Label("Excursions:");
			infoPane.add(lblExcursions, 0, 10);

			txfName = new TextField(r.getParticipant().getName());
			txfName.setEditable(false);
			infoPane.add(txfName, 1, 0);

			txfID = new TextField(Integer.toString(r.getID()));
			txfID.setEditable(false);
			infoPane.add(txfID, 1, 1);

			txfPhoneNr = new TextField(Integer.toString(r.getParticipant().getPhoneNr()));
			txfPhoneNr.setEditable(false);
			infoPane.add(txfPhoneNr, 1, 2);

			txfAddress = new TextField(r.getParticipant().getAddress());
			txfAddress.setEditable(false);
			infoPane.add(txfAddress, 1, 3);

			txfCompany = new TextField();
			txfCompany.setEditable(false);
			txfCompanyPhoneNr = new TextField();
			txfCompanyPhoneNr.setEditable(false);
			if (r.getCompany() != null) {
				txfCompany.setText(r.getCompany().getName());
				txfCompanyPhoneNr.setText(r.getCompany().getPhoneNr() + "");
			}
			infoPane.add(txfCompany, 1, 4);
			infoPane.add(txfCompanyPhoneNr, 2, 4);

			txfCompanion = new TextField();
			txfCompanion.setEditable(false);
			if (r.getCompanion() != "") {
				txfCompanion.setText(r.getCompanion());
			}
			infoPane.add(txfCompanion, 1, 5);

			datePickerStart = new DatePicker();
			datePickerStart.setDisable(true);
			datePickerStart.setValue(conference.getStartDate());
			infoPane.add(datePickerStart, 1, 6);

			datePickerEnd = new DatePicker();
			datePickerEnd.setDisable(true);
			datePickerEnd.setValue(conference.getEndDate());
			infoPane.add(datePickerEnd, 2, 6);

			txfHotel = new TextField();
			txfHotel.setEditable(false);
			if (r.getHotel() != null) {
				txfHotel.setText(r.getHotel().getName());
			}
			infoPane.add(txfHotel, 1, 7);

			if (!r.getServices().isEmpty()) {
				lvwServices.getItems().setAll(r.getServices());
				lvwServices.setDisable(true);
				infoPane.add(lvwServices, 1, 8);
			}

			txfPrice = new TextField(Double.toString(r.calculatePrice()));
			txfPrice.setEditable(false);
			infoPane.add(txfPrice, 1, 9);

			infoPane.add(lvwExcursions, 1, 10, 1, 1);

			lvwExcursions.getItems().setAll(r.getExcursions());
			lvwExcursions.setDisable(true);
		}
	}

	private void editAction(Conference conference) {
		if (lvwRegistrations.getSelectionModel().getSelectedItem() != null) {

			txfName.setEditable(true);
			txfPhoneNr.setEditable(true);
			txfAddress.setEditable(true);
			txfCompanion.setEditable(true);
			cbxSpeaker.setDisable(false);

			Registration r = lvwRegistrations.getSelectionModel().getSelectedItem();

			btnEdit.setId("save");
			btnEdit.setOnAction(event -> updateAction(conference));

			infoPane.getChildren().remove(txfCompany);

			cmbCompany = new ComboBox<>(FXCollections.observableArrayList(Storage.getCompanies()));
			infoPane.add(cmbCompany, 1, 4);
			cmbCompany.setOnAction(event -> onCompanyChange());

			infoPane.getChildren().remove(txfHotel);

			cmbHotels = new ComboBox<>(FXCollections.observableArrayList(conference.getHotels()));
			infoPane.add(cmbHotels, 1, 7);
			cmbHotels.setOnAction(event -> onHotelChange());

			infoPane.getChildren().remove(lvwServices);
			ccbServices = new CheckComboBox<>();
			if (chosenHotel != null) {
				ccbServices.getItems().setAll(chosenHotel.getServices());
			}
			infoPane.add(ccbServices, 1, 8);

			infoPane.getChildren().remove(lvwExcursions);
			ccbExcursions = new CheckComboBox<>();
			ccbExcursions.getItems().setAll(conference.getExcursions());
			infoPane.add(ccbExcursions, 1, 10);

		}
	}

	private void updateAction(Conference conference) {
		Registration registration = lvwRegistrations.getSelectionModel().getSelectedItem();

		Participant participant = registration.getParticipant();
		if (participant.getName() != txfName.getText() || participant.getAddress() != txfAddress.getText()
				|| participant.getPhoneNr() != Integer.parseInt(txfPhoneNr.getText())) {
			ControllerStorage.updateParticipant(registration.getParticipant(), txfName.getText(), txfAddress.getText(),
					Integer.parseInt(txfPhoneNr.getText()));
		}

		Hotel hotel = registration.getHotel();
		if (hotel == null || hotel != cmbHotels.getSelectionModel().getSelectedItem()) {
			hotel = null;
			for (Hotel h : conference.getHotels()) {
				if (h == cmbHotels.getSelectionModel().getSelectedItem()) {
					hotel = h;
					registration.getServices().clear();
				}
			}
		}

		for (Service s : registration.getServices()) {
			ControllerStorage.removeServiceFromRegistration(registration, s);
		}

		if (hotel != null) {
			for (Service s : ccbServices.getCheckModel().getCheckedItems()) {
				ControllerStorage.addServiceToRegistration(registration, s);
			}
		}

		Company company = registration.getCompany();
		if (company == null || company != cmbCompany.getSelectionModel().getSelectedItem()) {
			for (Company c : Storage.getCompanies()) {
				if (c == cmbCompany.getSelectionModel().getSelectedItem()) {
					company = c;
				}
			}
		}

		Boolean speaker = registration.isSpeaker();
		if (speaker != cbxSpeaker.isSelected()) {
			speaker = cbxSpeaker.isSelected();
		}

		for (Excursion e : registration.getExcursions()) {
			ControllerStorage.removeCompanionFromExcursion(registration, e);
		}

		if (txfCompanion.getText() != "") {
			for (Excursion e : ccbExcursions.getCheckModel().getCheckedItems()) {
				ControllerStorage.addExcursionToRegistration(registration, e);
			}
		}

		String companion = txfCompanion.getText();

		ControllerStorage.updateRegistration(registration, participant, conference, hotel, company, speaker, companion);
		btnBox.getChildren().remove(btnBox.lookup("#update"));
		participantItemSelected(conference);
		updateParticipantList();
	}

	public void updateParticipantList() {
		lvwRegistrations.getItems().setAll(conference.getRegistrations());
	}

	public void onHotelChange() {
		if (cmbHotels.getSelectionModel().getSelectedItem() != null
				&& cmbHotels.getSelectionModel().getSelectedItem() != "None") {
			chosenHotel = (Hotel) cmbHotels.getSelectionModel().getSelectedItem();
			ObservableList<Service> services = FXCollections.observableArrayList(chosenHotel.getServices());
			ccbServices.getItems().setAll(chosenHotel.getServices());

		}
	}

	public void onCompanyChange() {
		if (cmbCompany.getSelectionModel().getSelectedItem() != null) {
			Company company = (Company) cmbCompany.getSelectionModel().getSelectedItem();
			txfCompanyPhoneNr.setText(Integer.toString(company.getPhoneNr()));
		}
	}

	private void deleteAction(Conference conference) {
		ControllerStorage.removeRegistrationFromConference(lvwRegistrations.getSelectionModel().getSelectedItem(),
				conference);
		lvwRegistrations.getItems().setAll(conference.getRegistrations());
	}
	
	private void printActionAction() {
		if (lvwRegistrations.getSelectionModel().getSelectedItem() != null) {
			ControllerGUI.onActionClickedPrint(lvwRegistrations.getSelectionModel().getSelectedItem());
		}
	}
}
