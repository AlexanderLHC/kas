package application.model;

import java.util.ArrayList;

public class Hotel {
	private String name;
	private String address;
	private double pricePerDaySingle;
	private double pricePerDayDouble;
	private ArrayList<Service> services = new ArrayList<>();
	private ArrayList<Registration> registrations = new ArrayList<>();

	public Hotel(String name, String address, double pricePerDaySingle, double pricePerDayDouble) {
		this.name = name;
		this.address = address;
		this.pricePerDaySingle = pricePerDaySingle;
		this.pricePerDayDouble = pricePerDayDouble;
	}

	public Service createService(String name, double price) {
		Service service = new Service(name, this, price);
		services.add(service);
		return service;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public double getPricePerDaySingle() {
		return pricePerDaySingle;
	}

	public double getPricePerDayDouble() {
		return pricePerDayDouble;
	}

	public ArrayList<Registration> getRegistrations() {
		return new ArrayList<>(registrations);
	}

	public ArrayList<Service> getServices() {
		return new ArrayList<>(services);
	}

	public ArrayList<Participant> getParticipants() {
		ArrayList<Participant> participants = new ArrayList<>();
		for (Registration r : registrations) {
			participants.add(r.getParticipant());
		}
		return participants;
	}

	public void addService(Service service) {
		if (!services.contains(service)) {
			services.add(service);
		}
	}

	public void addregistration(Registration registration) {
		if (!registrations.contains(registration)) {
			registrations.add(registration);
			registration.setHotel(this);
		}
	}

	public void removeService(Service service) {
		if (services.contains(service)) {
			services.remove(service);
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPricePerDaySingle(double pricePerDaySingle) {
		this.pricePerDaySingle = pricePerDaySingle;
	}

	public void setPricePerDayDouble(double pricePerDayDouble) {
		this.pricePerDayDouble = pricePerDayDouble;
	}

	public void removeRegistration(Registration registration) {
		if (registrations.contains(registration)) {
			registrations.remove(registration);
			registration.setHotel(null);
		}
	}

	public String toString() {
		return name;
	}
}
