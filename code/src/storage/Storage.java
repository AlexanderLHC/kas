package storage;

import java.time.LocalDate;
import java.util.ArrayList;

import application.model.Company;
import application.model.Conference;
import application.model.Excursion;
import application.model.Hotel;
import application.model.Participant;
import application.model.Registration;
import application.model.Service;

public class Storage {
	private static final ArrayList<Conference> conferences = new ArrayList<>();
	private static final ArrayList<Hotel> hotels = new ArrayList<>();
	private static final ArrayList<Excursion> excursions = new ArrayList<>();
	private static final ArrayList<Participant> participants = new ArrayList<>();
	private static final ArrayList<Registration> registrations = new ArrayList<>();
	private static final ArrayList<Company> companies = new ArrayList<>();
	private static final ArrayList<Service> services = new ArrayList<>();

	// --------------------------- Conferences ---------------------------
	public static ArrayList<Conference> getConferences() {
		return new ArrayList<Conference>(conferences);
	}

	public static void addConference(Conference conference) {
		conferences.add(conference);
	}

	public static void updateConference(Conference conference, String name, int tickets, LocalDate startDate,
			LocalDate endDate,
			double pricePerDay, String location) {
		conference.setName(name);
		conference.setTickets(tickets);
		conference.setStartDate(startDate);
		conference.setEndDate(endDate);
		conference.setPricePerDay(pricePerDay);
		conference.setLocation(location);
	}

	public static void removeConference(Conference conference) {
		conferences.remove(conference);
	}

	// --------------------------- Hotels ---------------------------
	public static ArrayList<Hotel> getHotels() {
		return new ArrayList<Hotel>(hotels);
	}

	public static void addHotel(Hotel hotel) {
		hotels.add(hotel);
	}

	public static void updateHotel(Hotel hotel, String name, String address, double pricePerDaySingle,
			double pricePerDayDouble) {

		hotel.setName(name);
		hotel.setAddress(address);
		hotel.setPricePerDaySingle(pricePerDaySingle);
		hotel.setPricePerDayDouble(pricePerDayDouble);
	}

	public static void removeHotel(Hotel hotel) {
		hotels.remove(hotel);
	}

	// --------------------------- Excursions ---------------------------
	public static ArrayList<Excursion> getExcursions() {
		return new ArrayList<Excursion>(excursions);
	}

	public static void addExcursion(Excursion excursion) {
		excursions.add(excursion);
	}

	public static void removeExcursion(Excursion excursion) {
		excursions.remove(excursion);
	}

	// --------------------------- Participants ---------------------------
	public static ArrayList<Participant> getParticipants() {
		return new ArrayList<Participant>(participants);
	}

	public static void addParticipant(Participant participant) {
		participants.add(participant);
	}

	public static void removeParticipant(Participant participant) {
		participants.remove(participant);
	}

	// --------------------------- Registrations ---------------------------
	public static ArrayList<Registration> getRegistrations() {
		return new ArrayList<Registration>(registrations);
	}

	public static void addRegistration(Registration registration) {
		registrations.add(registration);
	}

	public static void removeRegistration(Registration registration) {
		registrations.remove(registration);
	}

	// --------------------------- Companies ---------------------------
	public static ArrayList<Company> getCompanies() {
		return new ArrayList<Company>(companies);
	}

	public static void addCompany(Company company) {
		companies.add(company);
	}

	public static void removeCompany(Company company) {
		companies.remove(company);
	}

	// --------------------------- Services ---------------------------
	public static ArrayList<Service> getServices() {
		return new ArrayList<Service>(services);
	}

	public static void addService(Service service) {
		services.add(service);
	}

	public static void removeService(Service service) {
		services.remove(service);
	}

}
